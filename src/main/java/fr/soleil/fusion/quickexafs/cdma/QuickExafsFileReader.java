package fr.soleil.fusion.quickexafs.cdma;

import java.io.File;
import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Properties;

import org.cdma.IFactory;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.exception.DataAccessException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IDataItem;
import org.slf4j.LoggerFactory;

import fr.soleil.cdma.box.reader.CDMAFileReader;
import fr.soleil.cdma.box.view.FileView;
import fr.soleil.fusion.quickexafs.model.MonoUtils;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;
import fr.soleil.lib.project.math.UnsignedConverter;

public class QuickExafsFileReader extends CDMAFileReader {

    private static final String QUICK_EXAFS_DICTIONARY = "quick_exafs_view.xml";
    private static final String QUICK_EXAFS_DICTIONARY_VIEW = "quick_exafs";
    private static final String QUICK_EXAFS_PACKAGE = "/fr/soleil/fusion/quickexafs/dictionary/";

    private static final String DELTA_THETA = "delta_theta";
    private static final String INTENSITY_0 = "intensity0";
    private static final String INTENSITY_1 = "intensity1";
    private static final String INTENSITY_2 = "intensity2";
    private static final String FLUO = "fluo";
    private static final String MONOCHROMATOR_NAME = "monochromator_name";
    private static final String CRISTAL_INTERRETICULAR_DISTANCE = "cristal_inter_reticular_distance";
    private static final String DELTA_THETA0 = "delta_theta0";
    private static final String THETA = "theta";

    private static final String THRESHOLD_VALUE = "THRESHOLD_VALUE";
    private static final String THRESHOLD_DEFAULT_VALUE = "10000";
    private static final String THRESHOLD_WIDTH = "THRESHOLD_WIDTH";
    private static final String THRESHOLD_DEFAULT_WIDTH = "200";

    private static final String FILE_SELECTED = "File selected: ";
    private static final String MONO_KEY_NOT_INITIALIZED = "Mono key not initialized!";
    private static final String ACQUISITION_LOADING_INTERRUPTED = "Acquisition loading interrupted";
    private static final String FAILED_TO_COMPUTE_EDGE_INDEX_FROM = "Failed to compute edge index from ";
    private static final String FAILED_TO_LOAD_DATA_FROM = "Failed to load data from ";
    private static final String AT_INDEX = " at index = ";
    private static final String LOADED_DATA = "Loaded data: @";
    private static final String LOADED_DATA_LENGTH = "\nLoaded data length: ";
    private static final String THETA_LOADING_INTERRUPTED = "Theta loading interrupted";
    private static final String DELTA_THETA0_LOADING_INTERRUPTED = "Delta Theta0 loading interrupted";

    private static final Collator COLLATOR = Collator.getInstance();

    public static final int DEFAULT_REFERENT_VALUE = 25000;

    private double[] deltaTheta;
    private double[] intensity0;
    private double[] intensity1;
    private double[] intensity2;
    private double[] fluo;

    private long lastModification;

    private int edgeIndex;
    private int[] upwardEdges, backwardEdges;
    private int[] mergedEdges;
    private int upwardCount, backwardCount;
    private final Properties properties;
    private String monoKey;
    private double referentValue;
    private boolean alternateAlgorithm;
    private int preferedEdgeIndex;
    private final Object lock;
    private double thresholdValue, thresholdWidth;

    public QuickExafsFileReader(String applicationId, Properties properties, boolean alternateAlgorithm,
            double thresholdValue, double thresholdWidth) {
        super(applicationId);
        lock = new Object();
        setDictionaryView(QUICK_EXAFS_DICTIONARY_VIEW);
        setMandatoryDictionary(QUICK_EXAFS_DICTIONARY);
        setMandatoryDictionaryPackage(QUICK_EXAFS_PACKAGE);
        this.properties = properties;
        upwardEdges = new int[10];
        backwardEdges = new int[10];
        upwardCount = 0;
        backwardCount = 0;
        mergedEdges = ArrayUtils.EMPTY_SHAPE;
        referentValue = DEFAULT_REFERENT_VALUE;
        preferedEdgeIndex = 2;
        deltaTheta = null;
        intensity0 = null;
        intensity1 = null;
        intensity2 = null;
        fluo = null;
        lastModification = -1;
        this.alternateAlgorithm = alternateAlgorithm;
        setThresholdValue(thresholdValue);
        setThresholdWidth(thresholdWidth);
    }

    @Override
    protected void loadExperimentConfigNoLock(IFactory factory) {
        checkDictionary();
        updateReaderAccordingToFactoy(factory);
        // NO CONFIG FOR QUICKEXAFS
    }

    public int getPreferedEdgeIndex() {
        return preferedEdgeIndex;
    }

    public void setPreferedEdgeIndex(int preferedEdgeIndex) {
        this.preferedEdgeIndex = Math.max(0, preferedEdgeIndex);
    }

    public void resetIndex() {
        edgeIndex = 0;
    }

    @Override
    public void setFile(File file) {
        setFile(file, true);
    }

    public void setFile(File file, boolean loadEdges) {
        if ((file != null) && (!ObjectUtils.sameObject(getWorkingPath(), file.getAbsolutePath()))) {
            synchronized (lock) {
                LoggerFactory.getLogger(applicationId).info(FILE_SELECTED + file.getName());
                closeNoLock();
                setDictionaryView(QUICK_EXAFS_DICTIONARY_VIEW);
                setMandatoryDictionary(QUICK_EXAFS_DICTIONARY);
                setMandatoryDictionaryPackage(QUICK_EXAFS_PACKAGE);
                super.setFile(file);
                lastModification = -1;
                deltaTheta = null;
                intensity0 = null;
                intensity1 = null;
                intensity2 = null;
                fluo = null;

                if (loadEdges) {
                    loadEdges();
                } else {
                    resetIndex();
                }
            }
        }
    }

    private boolean isUptoDate() {
        boolean upToDate;
        if ((reader == null) || (reader.isDead())) {
            upToDate = false;
        } else {
            long last = reader.getLastModificationDate();
            upToDate = (lastModification == last);
            if (!upToDate) {
                lastModification = last;
            }
        }
        return upToDate;
    }

    private LogicalGroup getLogicalRoot() {
        LogicalGroup root;
        synchronized (lock) {
            if (!isUptoDate()) {
                closeNoLock();
                reader = null;
                updateReader();
            }
            root = (reader == null ? null : reader.getLogicalRoot(QUICK_EXAFS_DICTIONARY_VIEW));
        }
        return root;
    }

    public void close() {
        synchronized (lock) {
            closeNoLock();
        }
    }

    protected void closeNoLock() {
        if ((reader != null) && reader.isOpen()) {
            try {
                reader.close();
            } catch (DataAccessException e) {
                LoggerFactory.getLogger(applicationId).error(getCloseUriErrorMessage(), e);
            }
        }
    }

    public void loadEdges() {
        resetIndex();
        upwardCount = 0;
        backwardCount = 0;
        synchronized (lock) {
            checkDictionary();
            if (monoKey == null) {
                loadMono();
            }
            if (monoKey == null) {
                LoggerFactory.getLogger(applicationId).error(MONO_KEY_NOT_INITIALIZED);
            } else {
                if (!isCanceled()) {
                    double[] data = null;
                    IFactory factory = getFactory();
                    if ((reader != null) && (factory != null)) {
                        try {
                            if (!reader.isOpen()) {
                                reader.open();
                            }
                            try {
                                LogicalGroup logicalRoot = getLogicalRoot();
                                if (logicalRoot != null) {
                                    IDataItem directionItem = logicalRoot.getDataItem(monoKey);
                                    if (directionItem != null) {
                                        // Edges reading
                                        IArray directionArray = directionItem.getData();
                                        Object direction = directionArray.getArrayUtils().copyTo1DJavaArray();
                                        if (directionItem.isUnsigned()) {
                                            // Unsigned integers conversion
                                            direction = UnsignedConverter.convertUnsigned(direction);
                                        }
                                        data = NumberArrayUtils.extractDoubleArray(direction);
                                    }
                                }
                            } catch (Exception e) {
                                LoggerFactory.getLogger(applicationId).error(ACQUISITION_LOADING_INTERRUPTED, e);
                            }
                        } catch (Exception e) {
                            LoggerFactory.getLogger(applicationId).error(ACQUISITION_LOADING_INTERRUPTED, e);
                        }
                    }
                    if (alternateAlgorithm) {
                        findEdges2(data);
                    } else {
                        findEdges(data, thresholdValue, thresholdWidth);
                    }
                }
            }
        }
    }

    public boolean hasNextValues(boolean upward, boolean fromPreferedEdgeIndex) {
        boolean result;
        int count = upward ? upwardCount : backwardCount;
        if (count == 0) {
            result = false;
        } else {
            result = edgeIndex < count;
            if (result && !alternateAlgorithm) {
                Integer fromIndexInMergedList = (Integer) loadQuickExafsDataOrGetFromIndex(upward,
                        fromPreferedEdgeIndex, true);
                if (fromIndexInMergedList != null) {
                    int index = fromIndexInMergedList.intValue();
                    result = (index > -1) && (index < mergedEdges.length - 1);
                }
            }
        }
        return result;
    }

    private void findEdges(double[] deltaTheta, double thresholdValue, double thresholdWidth) {
        int index = 0;
        int firstIndex = -1;
        int currentEdgesCount = 0;
        Double currentValue = null;
        upwardCount = 0;
        backwardCount = 0;
        if (deltaTheta != null) {
            for (double value : deltaTheta) {
                if (currentValue == null) {
                    currentValue = value;
                }
                if (Math.abs(currentValue - value) > thresholdValue) {
                    if (firstIndex == -1) {
                        firstIndex = index;
                    }
                    currentEdgesCount++;
                    if (currentEdgesCount > thresholdWidth) {
                        if ((currentValue - value) > 0) {
                            if (upwardCount >= upwardEdges.length) {
                                upwardEdges = Arrays.copyOf(upwardEdges, upwardEdges.length + 5);
                            }
                            upwardEdges[upwardCount++] = firstIndex;
                        } else {
                            if (backwardCount >= backwardEdges.length) {
                                backwardEdges = Arrays.copyOf(backwardEdges, backwardEdges.length + 5);
                            }
                            backwardEdges[backwardCount++] = firstIndex;
                        }
                        currentValue = value;
                        firstIndex = -1;
                        currentEdgesCount = 0;
                    }
                } else {
                    firstIndex = -1;
                    currentEdgesCount = 0;
                }
                index++;
            }
        }
        Arrays.sort(upwardEdges, 0, upwardCount);
        Arrays.sort(backwardEdges, 0, backwardCount);
        mergedEdges = new int[upwardCount + backwardCount];
        System.arraycopy(upwardEdges, 0, mergedEdges, 0, upwardCount);
        System.arraycopy(backwardEdges, 0, mergedEdges, upwardCount, backwardCount);
        Arrays.sort(mergedEdges);
    }

    private void findEdges2(double[] deltaTheta) {
        int index = 0;
        Double currentValue = null;
        Boolean up = null;
        upwardCount = 0;
        backwardCount = 0;
        if (deltaTheta != null) {
            for (double value : deltaTheta) {
                if (currentValue == null) {
                    currentValue = value;
                }
                if (value > referentValue) {
                    if ((up == null) || (!up.booleanValue())) {
                        boolean canAdd = true;
                        for (int i = 0; i < upwardCount && canAdd; i++) {
                            if (upwardEdges[i] == index) {
                                canAdd = false;
                            }
                        }
                        if (canAdd) {
                            if (upwardCount >= upwardEdges.length) {
                                upwardEdges = Arrays.copyOf(upwardEdges, upwardEdges.length + 5);
                            }
                            upwardEdges[upwardCount++] = index;
                        }
                    }
                    up = Boolean.TRUE;
                } else {
                    if ((up == null) || up.booleanValue()) {
                        boolean canAdd = true;
                        for (int i = 0; i < backwardCount && canAdd; i++) {
                            if (backwardEdges[i] == index) {
                                canAdd = false;
                            }
                        }
                        if (canAdd) {
                            if (backwardCount >= backwardEdges.length) {
                                backwardEdges = Arrays.copyOf(backwardEdges, backwardEdges.length + 5);
                            }
                            backwardEdges[backwardCount++] = index;
                        }
                    }
                    up = Boolean.FALSE;
                }
                index++;
            }
        }
        Arrays.sort(upwardEdges, 0, upwardCount);
        Arrays.sort(backwardEdges, 0, backwardCount);
        mergedEdges = new int[upwardCount + backwardCount];
        System.arraycopy(upwardEdges, 0, mergedEdges, 0, upwardCount);
        System.arraycopy(backwardEdges, 0, mergedEdges, upwardCount, backwardCount);
        Arrays.sort(mergedEdges);
    }

    public String loadMono() {
        String mono = null;
        synchronized (lock) {
            IFactory factory = getFactory();
            updateReaderAccordingToFactoy(factory);
            if ((reader != null) && (factory != null)) {
                try {
                    if (!reader.isOpen()) {
                        reader.open();
                    }
                    LogicalGroup logicalRoot = getLogicalRoot();
                    if (logicalRoot != null) {
                        IDataItem monoNameItem = logicalRoot.getDataItem(MONOCHROMATOR_NAME);
                        if (monoNameItem != null) {
                            mono = monoNameItem.readScalarString();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LoggerFactory.getLogger(applicationId).error(ACQUISITION_LOADING_INTERRUPTED, e);
                }
            }
            if (mono != null) {
                monoKey = MonoUtils.findMonoDirectionKey(mono);
            }
        }
        return mono;
    }

    public double[][] loadQuickExafsData(boolean upward, boolean fromPreferedEdgeIndex) {
        return (double[][]) loadQuickExafsDataOrGetFromIndex(upward, fromPreferedEdgeIndex, false);
    }

    protected Object loadQuickExafsDataOrGetFromIndex(boolean upward, boolean fromPreferedEdgeIndex,
            boolean getFromIndex) {
        Object result = null;
        int[] edges;
        int count;
        if (upward) {
            edges = upwardEdges;
            count = upwardCount;
        } else {
            edges = backwardEdges;
            count = backwardCount;
        }
        if (count > 0) {
            try {
                if (fromPreferedEdgeIndex) {
                    if (preferedEdgeIndex < 0) {
                        edgeIndex = 0;
                    } else if (preferedEdgeIndex >= count) {
                        edgeIndex = count - 1;
                    } else {
                        edgeIndex = preferedEdgeIndex;
                    }
                }
                if (edgeIndex < count) {
                    int fromIndex = edges[edgeIndex];

                    int fromIndexInMergedList = indexOf(fromIndex, mergedEdges, mergedEdges.length);
                    if (getFromIndex) {
                        result = Integer.valueOf(fromIndexInMergedList);
                    } else {
                        int toIndex;
                        if (fromIndexInMergedList < mergedEdges.length - 1) {
                            toIndex = mergedEdges[fromIndexInMergedList + 1];
                            result = loadQuickExafsData(fromIndex, toIndex - 1);
                        } else {
                            result = null;
                        }
                    }
                }
            } catch (Exception e) {
                String message;
                if (getFromIndex) {
                    message = FAILED_TO_COMPUTE_EDGE_INDEX_FROM + getWorkingPath();
                } else {
                    message = FAILED_TO_LOAD_DATA_FROM + getWorkingPath() + AT_INDEX + edgeIndex;
                }
                LoggerFactory.getLogger(applicationId).error(message, e);
            } finally {
                if ((!getFromIndex) && (!fromPreferedEdgeIndex)) {
                    edgeIndex++;
                }
            }
        }
        return result;
    }

    protected int indexOf(int value, int[] array, int length) {
        int index = -1;
        for (int i = 0; i < length; i++) {
            if (array[i] == value) {
                index = i;
                break;
            }
        }
        return index;
    }

    public double[][] loadQuickExafsData(int from, int to) {
        double[][] result = new double[5][];
        synchronized (lock) {
            checkDictionary();
            if (!isCanceled()) {
                IFactory factory = getFactory();
                if ((reader != null) && (factory != null)) {
                    try {
                        if (!reader.isOpen()) {
                            reader.open();
                        }
                        try {
                            LogicalGroup logicalRoot = getLogicalRoot();
                            if (logicalRoot != null) {
                                IDataItem deltaThetaItem = logicalRoot.getDataItem(DELTA_THETA);
                                IDataItem intensity0Item = logicalRoot.getDataItem(INTENSITY_0);
                                IDataItem intensity1Item = logicalRoot.getDataItem(INTENSITY_1);
                                IDataItem intensity2Item = logicalRoot.getDataItem(INTENSITY_2);
                                IDataItem fluoItem = logicalRoot.getDataItem(FLUO);
                                if ((deltaThetaItem != null) && (intensity0Item != null) && (intensity1Item != null)) {
                                    if (deltaTheta == null) {
                                        IArray deltaThetaArray = deltaThetaItem.getData();
                                        deltaTheta = (double[]) deltaThetaArray.getArrayUtils().copyTo1DJavaArray();
                                    }
                                    if (intensity0 == null) {
                                        intensity0 = getIntensity(intensity0Item);
                                    }
                                    if (intensity1 == null) {
                                        intensity1 = getIntensity(intensity1Item);
                                    }
                                    if (intensity2 == null) {
                                        intensity2 = getIntensity(intensity2Item);
                                    }
                                    if (fluo == null) {
                                        fluo = getIntensity(fluoItem);
                                    }
                                    result[0] = copyOfRange(deltaTheta, from, to);
                                    result[1] = copyOfRange(intensity0, from, to);
                                    result[2] = copyOfRange(intensity1, from, to);
                                    result[3] = copyOfRange(intensity2, from, to);
                                    result[4] = copyOfRange(fluo, from, to);
                                }
                            }
                        } catch (Exception e) {
                            LoggerFactory.getLogger(applicationId).error(ACQUISITION_LOADING_INTERRUPTED, e);
                        }
                    } catch (Exception e) {
                        LoggerFactory.getLogger(applicationId).error(ACQUISITION_LOADING_INTERRUPTED, e);
                    }
                }
            }
        }
        LoggerFactory.getLogger(applicationId).trace(new StringBuilder(LOADED_DATA).append(from).append("-> @")
                .append(to).append(LOADED_DATA_LENGTH).append((to - from + 1)).toString());
        return result;
    }

    protected double[] copyOfRange(double[] array, int from, int to) {
        double[] result;
        if ((array == null) || (from < 0) || (from >= array.length) || (to <= from)) {
            result = null;
        } else if (to > array.length) {
            result = Arrays.copyOfRange(array, from, array.length);
        } else {
            result = Arrays.copyOfRange(array, from, to);
        }
        return result;
    }

    protected boolean isRangeOk(double[] array, int from, int to) {
        boolean rangeOk;
        if (array == null) {
            rangeOk = false;
        } else if ((from < 0) || (from >= array.length)) {
            rangeOk = false;
        } else if ((to <= from) || (to > array.length)) {
            rangeOk = false;
        } else {
            rangeOk = true;
        }
        return rangeOk;
    }

    protected double[] getIntensity(IDataItem intensityItem) throws Exception {
        double[] intensity = null;
        if (intensityItem != null) {
            IArray intensityArray = intensityItem.getData();
            Object tmp = intensityArray.getArrayUtils().copyTo1DJavaArray();
            if (intensityItem.isUnsigned()) {
                tmp = UnsignedConverter.convertUnsigned(tmp);
            }
            intensity = NumberArrayUtils.extractDoubleArray(tmp);
        }
        return intensity;
    }

    public Double loadInterreticularDistance() {
        Double result = null;
        synchronized (lock) {
            IFactory factory = getFactory();
            if ((reader != null) && (factory != null)) {
                try {
                    if (!reader.isOpen()) {
                        reader.open();
                    }
                    try {
                        LogicalGroup logicalRoot = getLogicalRoot();
                        if (logicalRoot != null) {
                            IDataItem distanceItem = logicalRoot.getDataItem(CRISTAL_INTERRETICULAR_DISTANCE);
                            if (distanceItem != null) {
                                IArray distanceArray = distanceItem.getData();
                                if (distanceArray != null) {
                                    double[] values = (double[]) distanceArray.getArrayUtils().copyTo1DJavaArray();
                                    if ((values != null) && (values.length > 0)) {
                                        result = Double.valueOf(values[0]);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        LoggerFactory.getLogger(applicationId).error(ACQUISITION_LOADING_INTERRUPTED, e);
                    }
                } catch (Exception e) {
                    LoggerFactory.getLogger(applicationId).error(ACQUISITION_LOADING_INTERRUPTED, e);
                }
            }
        }
        return result;
    }

    public Double loadTheta() {
        Double result = null;
        synchronized (lock) {
            IFactory factory = getFactory();
            if ((reader != null) && (factory != null)) {
                try {
                    if (!reader.isOpen()) {
                        reader.open();
                    }
                    try {
                        LogicalGroup logicalRoot = getLogicalRoot();
                        if (logicalRoot != null) {
                            IDataItem distanceItem = logicalRoot.getDataItem(THETA);
                            if (distanceItem != null) {
                                IArray distanceArray = distanceItem.getData();
                                if (distanceArray != null) {
                                    double[] values = (double[]) distanceArray.getArrayUtils().copyTo1DJavaArray();
                                    if ((values != null) && (values.length > 0)) {
                                        result = Double.valueOf(values[0]);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        LoggerFactory.getLogger(applicationId).error(THETA_LOADING_INTERRUPTED, e);
                    }
                } catch (Exception e) {
                    LoggerFactory.getLogger(applicationId).error(THETA_LOADING_INTERRUPTED, e);
                }
            }
        }
        return result;
    }

    public Double loadDeltaTheta0() {
        Double result = null;
        synchronized (lock) {
            IFactory factory = getFactory();
            if ((reader != null) && (factory != null)) {
                try {
                    if (!reader.isOpen()) {
                        reader.open();
                    }
                    try {
                        LogicalGroup logicalRoot = getLogicalRoot();
                        if (logicalRoot != null) {
                            IDataItem distanceItem = logicalRoot.getDataItem(DELTA_THETA0);
                            if (distanceItem != null) {
                                IArray distanceArray = distanceItem.getData();
                                if (distanceArray != null) {
                                    double[] values = (double[]) distanceArray.getArrayUtils().copyTo1DJavaArray();
                                    if ((values != null) && (values.length > 0)) {
                                        result = Double.valueOf(values[0]);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        LoggerFactory.getLogger(applicationId).error(DELTA_THETA0_LOADING_INTERRUPTED, e);
                    }
                } catch (Exception e) {
                    LoggerFactory.getLogger(applicationId).error(DELTA_THETA0_LOADING_INTERRUPTED, e);
                }
            }
        }
        return result;
    }

    public File getFirstFileInDir(String workingDir, FileView view) {
        File fileToLoad;
        File[] files = sortFiles(workingDir);
        if (view != null) {
            files = view.filterFiles(files);
        }
        if ((files != null) && (files.length > 0)) {
            fileToLoad = files[0];
        } else {
            fileToLoad = null;
        }
        return fileToLoad;
    }

    public File[] sortFiles(String workingDir) {
        File[] result;
        if ((workingDir != null) && (!workingDir.isEmpty())) {
            File directory = new File(workingDir);
            if (directory.isDirectory()) {
                result = directory.listFiles();
                sortFiles(result);
            } else {
                result = null;
            }
        } else {
            result = null;
        }
        return result;
    }

    public void sortFiles(File[] files) {
        if ((files != null) && (files.length > 0)) {
            Arrays.sort(files, new Comparator<File>() {
                @Override
                public int compare(File f1, File f2) {
                    return COLLATOR.compare(f1.getName(), f2.getName());
                }
            });
        }
    }

    public void setMonoKey(String monoKey) {
        this.monoKey = monoKey;
    }

    public double getReferentValue() {
        return referentValue;
    }

    public void setReferentValue(double referentValue) {
        this.referentValue = referentValue;
    }

    public boolean isAlternateAlgorithm() {
        return alternateAlgorithm;
    }

    public void setAlternateAlgorithm(boolean alternateAlgorithm) {
        this.alternateAlgorithm = alternateAlgorithm;
    }

    public double getThresholdValue() {
        return thresholdValue;
    }

    public void setThresholdValue(double thresholdValue) {
        if (Double.isNaN(thresholdValue) || Double.isInfinite(thresholdValue) || thresholdValue <= 0) {
            this.thresholdValue = Double.parseDouble(properties.getProperty(THRESHOLD_VALUE, THRESHOLD_DEFAULT_VALUE));
        } else {
            this.thresholdValue = thresholdValue;
        }
    }

    public double getThresholdWidth() {
        return thresholdWidth;
    }

    public void setThresholdWidth(double thresholdWidth) {
        if (Double.isNaN(thresholdWidth) || Double.isInfinite(thresholdWidth) || thresholdWidth <= 0) {
            this.thresholdWidth = Double.parseDouble(properties.getProperty(THRESHOLD_WIDTH, THRESHOLD_DEFAULT_WIDTH));
        } else {
            this.thresholdWidth = thresholdWidth;
        }
    }

}
