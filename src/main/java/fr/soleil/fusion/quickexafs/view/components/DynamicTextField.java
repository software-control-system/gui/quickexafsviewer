package fr.soleil.fusion.quickexafs.view.components;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.Format;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.fusion.quickexafs.model.QuickExafsModel;
import fr.soleil.lib.project.ObjectUtils;

public class DynamicTextField extends JTextField {

    private static final long serialVersionUID = 7883079579760810058L;

    private static final String FORMAT_ERROR_FOR_TEXT = "{}: format error for text {} - {}";
    private static final String ILLEGAL_ACCESS_EXCEPTION = "IllegalAccessException";
    private static final String ILLEGAL_ARGUMENT_EXCEPTION = "IllegalArgumentException";
    private static final String INVOCATION_TARGET_EXCEPTION = "InvocationTargetException";
    private static final String BAD_LOCATION_EXCEPTION = "BadLocationException";

    private final QuickExafsModel model;
    private final Format decFormat = new DecimalFormat("#,##0.00000;#,##0.00000");
    protected final String applicationId;

    public DynamicTextField(final String applicationId, final QuickExafsModel model, final Method methodToExecute) {
        this.model = model;
        this.applicationId = applicationId == null ? Mediator.LOGGER_ACCESS : applicationId;
        getDocument().addDocumentListener(new SmartDocumentListener(this, methodToExecute, true));
    }

    public void setValue(final Double value) {
        if (value != null) {
            super.setText(decFormat.format(value));
        } else {
            super.setText(ObjectUtils.EMPTY_STRING);
        }
    }

    protected class SmartDocumentListener implements DocumentListener {

        private final Method method;
        private final JTextField textField;
        private final Color defaultColor;
        private final boolean isDouble;

        public SmartDocumentListener(final JTextField tf, final Method method) {
            this(tf, method, true);
        }

        public SmartDocumentListener(final JTextField tf, final Method method, final boolean isDouble) {
            this.method = method;
            this.textField = tf;
            this.defaultColor = tf.getBackground();
            this.isDouble = isDouble;
        }

        private void updateData(final DocumentEvent e) {
            final Logger logger = LoggerFactory.getLogger(applicationId);
            textField.setBackground(defaultColor);
            try {
                final String text = e.getDocument().getText(0, e.getDocument().getLength());
                try {
                    if ((text != null) && (!text.trim().isEmpty())) {
                        if (isDouble) {
                            method.invoke(model, Double.parseDouble(text));
                        } else {
                            method.invoke(model, Long.parseLong(text));
                        }
                    }
                } catch (final NumberFormatException e1) {
                    textField.setBackground(Color.RED);
                    logger.error(FORMAT_ERROR_FOR_TEXT, textField.getName(), text, e1.getMessage());
                }
                // catch (final Exception e1) {
                // final StringWriter writer = new StringWriter();
                // // e1.printStackTrace(new PrintWriter(writer));
                // LoggerFactory.getLogger(applicationId).error(Level.SEVERE, writer.toString());
                // }
            } catch (final IllegalAccessException e1) {
                logger.error(ILLEGAL_ACCESS_EXCEPTION, e1);
            } catch (final IllegalArgumentException e1) {
                logger.error(ILLEGAL_ARGUMENT_EXCEPTION, e1);
            } catch (final InvocationTargetException e1) {
                logger.error(INVOCATION_TARGET_EXCEPTION, e1);
            } catch (final BadLocationException e1) {
                logger.error(BAD_LOCATION_EXCEPTION, e1);
            }
        }

        @Override
        public void removeUpdate(final DocumentEvent e) {
            updateData(e);
        }

        @Override
        public void insertUpdate(final DocumentEvent e) {
            updateData(e);
        }

        @Override
        public void changedUpdate(final DocumentEvent e) {
            updateData(e);
        }
    }
}
