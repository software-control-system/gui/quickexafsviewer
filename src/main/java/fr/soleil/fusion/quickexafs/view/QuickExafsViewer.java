package fr.soleil.fusion.quickexafs.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.function.Supplier;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import ch.qos.logback.classic.Level;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.cdma.box.listener.DirectoryListener;
import fr.soleil.cdma.box.manager.AcquisitionManager;
import fr.soleil.cdma.box.manager.IFileManager;
import fr.soleil.cdma.box.util.GUIUtilities;
import fr.soleil.cdma.box.util.comparator.NameFileNodeComparator;
import fr.soleil.cdma.box.view.FileView;
import fr.soleil.cdma.box.view.FileView.FileBrowsingMode;
import fr.soleil.cdma.box.view.model.FileTreeModel;
import fr.soleil.cdma.box.view.renderer.FileTreeRenderer;
import fr.soleil.cdma.box.view.tree.node.FileNode;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.controller.BasicFileTargetController;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.IFileTarget;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.fusion.quickexafs.cdma.QuickExafsFileReader;
import fr.soleil.fusion.quickexafs.model.IModelListener;
import fr.soleil.fusion.quickexafs.model.QuickExafsData;
import fr.soleil.fusion.quickexafs.model.QuickExafsModel;
import fr.soleil.fusion.quickexafs.view.components.DynamicTextField;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.logging.LogAppender;
import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.swing.Splash;
import fr.soleil.lib.project.swing.icons.DecorableIcon;
import fr.soleil.lib.project.swing.text.DocumentNumber;

public class QuickExafsViewer extends JPanel
        implements IFileTarget, IModelListener, DocumentListener, ActionListener, DirectoryListener {

    private static final long serialVersionUID = -6736625363617752110L;

    private static final String[] FILE_EXTENSIONS = new String[] { "nxs", "hdf", "h4", "hdf4", "he4", "h5", "hdf5",
            "he5", "he2", "edf" };

    public static final String TITLE = "QuickExafs Viewer";
    public static final String DELAY = "Delay";
    protected static final String ENERGY = "Energy";
    protected static final String MUX = "Mux";
    protected static final String INTER_RETICULAR_DISTANCE_LABEL = "Inter Reticular Dist.";
    protected static final String THETA_LABEL = "Theta";
    protected static final String DELTA_THETA0_LABEL = "Delta Theta0";
    protected static final String NO_FILE_SELECTED = "<NO FILE SELECTED>";
    protected static final String FIRST_SPECTRUM = "First Spectrum";
    protected static final String SPECTRUM_PREFIX = "Spectrum ";
    protected static final String FLUO_SUFFIX = " # fluo";
    protected static final String REFERENCE_SUFFIX = " # reference";
    protected static final String USER_HOME = "user.home";
    protected static final String DATA_RECORDER_DEVICE = "DATA_RECORDER_DEVICE";
    protected static final String SCRATCH_RECORDER_DEVICE = "SCRATCH_RECORDER_DEVICE";
    protected static final String TARGET_DIRECTORY_ATTRIBUTE = "TARGET_DIRECTORY_ATTRIBUTE";
    protected static final String DATA_MERGER_DEVICE = "DATA_MERGER_DEVICE";
    protected static final String SCRATCH_MODE_ATTRIBUTE = "SCRATCH_MODE_ATTRIBUTE";
    protected static final String ENERGY_CONVERSION_FACTOR = "ENERGY_CONVERSION_FACTOR";
    protected static final String REFRESHING_GROUP = QuickExafsViewer.class.getSimpleName();

    protected static final String CHECK_CURRENT_DIRECTORY_AND_CONTEXTUAL_DATA = ": check current directory and contextual data";
    protected static final String NO_REGISTERED_DATA_MERGER_IN_PROPERTIES = "no registered dataMerger in properties";
    protected static final String CONTEXTUAL_DATA_FOUND = "Contextual Data found: {}";
    protected static final String INTER_RETICULAR_DISTANCE_FOUND = "Found Inter Reticular Distance : {}";
    protected static final String DELTA_THETA0_FOUND = "Found DeltaTheta0 : {}";
    protected static final String MONO_FOUND = "Found Mono : {}";
    protected static final String CONTEXTUAL_DATA_MISSING = "Contextual Data Missing";
    protected static final String THETA_FOUND = "Found Theta : {}";
    protected static final String MONO_NOT_FOUND = "The system cannot find SelectedMono.\nYou have to set the value in the parameters.";
    protected static final String INTER_RETICULAR_DISTANCE_NOT_FOUND = "The system cannot find the interreticular distance.\nYou have to set the value in the parameters.";
    protected static final String THETA_NOT_FOUND = "The system cannot find Theta.\nYou have to set the value in the parameters.";
    protected static final String DELTA_THETA0_NOT_FOUND = "The system cannot find DeltaTheta0.\nYou have to set the value in the parameters.";
    protected static final String SEPARATOR = " - ";
    protected static final String OPEN_PARENTHESIS = " (";
    protected static final String RESET_ALL = "Reset all";
    protected static final String EXEC = "EXEC";
    protected static final String NEW_WORKING_DIRECTORY_IS = "New working directory is ";
    protected static final String STOP = "Stop";
    protected static final String CANCEL = "CANCEL";
    protected static final String START = "Start";
    protected static final String SNAP = "Snap";
    protected static final String UPDATE_PATH = "Updt. Path";
    protected static final String CLEAR = "Clear";
    protected static final String SET_INTER_RETICULAR_DISTANCE = "setInterReticularDistance";
    protected static final String SET_THETA = "setTheta";
    protected static final String SET_DELAY = "setDelay";
    protected static final String SHOW_FLUORESCENCE = "Show Fluorescence";
    protected static final String SHOW_REFERENCE = "Show Reference";
    protected static final String SHOW_FIRST_SPECTRUM = "Show 1st Spectrum";
    protected static final String DELETE_PREVIOUS_CURVES = "Del. prev. curves";
    protected static final String DELETE_PREVIOUS_CURVES_TOOLTIP = "<html><body>Delete previous curves.<br />If selected, 1 black curve will be displayed at a time.<br />Otherwise, colored curves will be cumulated in chart, with the possibility to remove them through right click in legend</body></html>";
    protected static final String ALTERNATE_ALGORITHM = "Alternate algorithm";
    protected static final String REFERENCE_VALUE = "Reference value";
    protected static final String THRESHOLD_VALUE = "Threshold value";
    protected static final String THRESHOLD_WIDTH = "Threshold width";
    protected static final String AUTO_REFRESH = "Auto refresh";
    protected static final String EDGE_INDEX = "Edge index";
    protected static final String EDGE_INDEX_TOOLTIP = "Index of the rising or falling edge, starting to 1";
    protected static final String DIRECTION_FILTER = "Direction Filter";
    protected static final String FORWARD = "Forward";
    protected static final String BACKWARD = "Backward";
    protected static final String MONO = "Mono";
    protected static final String RESET_REFERENT_VALUE = "Reset referent value";
    protected static final String RESET_THRESHOLD_VALUE = "Reset threshold value";
    protected static final String RESET_THRESHOLD_WIDTH = "Reset threshold width";
    protected static final String RESET_EDGE_INDEX = "Reset edge index";

    protected static final Insets PARAMETERS_INSETS = new Insets(2, 5, 0, 5);
    protected static final Insets PARAMETERS_INSETS_NO_GAP = new Insets(0, 5, 0, 5);

    protected static final ImageIcon SNAPSHOT_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/org/tango-project/tango-icon-theme/32x32/devices/camera-photo.png"));
    protected static final ImageIcon START_LOADING_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/org/tango-project/tango-icon-theme/32x32/actions/go-next.png"));
    protected static final ImageIcon STOP_LOADING_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/org/tango-project/tango-icon-theme/32x32/actions/process-stop.png"));
    protected static final ImageIcon CLEAR_LOG_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/org/tango-project/tango-icon-theme/22x22/actions/edit-clear.png"));
    protected static final ImageIcon DELAY_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/com/famfamfam/silk/time.png"));
    protected static final ImageIcon DISPLAY_FIRST_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/com/famfamfam/silk/chart_curve_add.png"));
    protected static final ImageIcon INTERRETICULAR_DISTANCE_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/com/famfamfam/silk/magnifier.png"));
    protected static final ImageIcon AUTO_REFRESH_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/com/famfamfam/silk/arrow_refresh.png"));
    protected static final ImageIcon REFRESH_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/org/tango-project/tango-icon-theme/32x32/actions/view-refresh.png"));
    protected static final ImageIcon DIRECTION_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/com/famfamfam/silk/arrow_switch.png"));
    protected static final ImageIcon THETA_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/com/famfamfam/silk/wrench.png"));
    protected static final ImageIcon DELTA_THETA0_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/com/famfamfam/silk/information.png"));
    protected static final ImageIcon MONO_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/com/famfamfam/silk/color_wheel.png"));
    protected static final ImageIcon ALGORITHM_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/fr/soleil/fusion/quickexafs/icons/operations.gif"));
    protected static final ImageIcon REFERENCE_VALUE_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/fr/soleil/fusion/quickexafs/icons/referenceValue.png"));
    protected static final ImageIcon THRESHOLD_VALUE_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/fr/soleil/fusion/quickexafs/icons/thresholdValue.png"));
    protected static final ImageIcon THRESHOLD_WIDTH_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/fr/soleil/fusion/quickexafs/icons/thresholdWidth.png"));
    protected static final ImageIcon DELETE_PREVIOUS_CURVES_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/com/famfamfam/silk/chart_line_delete.png"));
    protected static final ImageIcon EDGE_ICON = new ImageIcon(
            QuickExafsViewer.class.getResource("/fr/soleil/fusion/quickexafs/icons/squareSignal.png"));
    protected static final DecorableIcon TRANSMISSION_ICON;
    protected static final DecorableIcon FLUO_ICON;
    protected static final DecorableIcon REFERENCE_ICON;

    protected static final CometeColor TRANSMISSION_COLOR = CometeColor.BLACK;
    protected static final CometeColor FLUO_COLOR = new CometeColor(10, 100, 5);
    protected static final CometeColor REFERENCE_COLOR = new CometeColor(150, 10, 150);

    protected static final int DIRECTORY_CHECK_PERIOD = 2000;

    static {
        final URL chartIcon = QuickExafsViewer.class.getResource("/com/famfamfam/silk/chart_curve.png");
        TRANSMISSION_ICON = new DecorableIcon(chartIcon);
        TRANSMISSION_ICON.setDecoration(new ImageIcon(
                QuickExafsViewer.class.getResource("/fr/soleil/fusion/quickexafs/icons/transmission.gif")));
        TRANSMISSION_ICON.setDecorated(true);
        FLUO_ICON = new DecorableIcon(chartIcon);
        FLUO_ICON.setDecoration(
                new ImageIcon(QuickExafsViewer.class.getResource("/fr/soleil/fusion/quickexafs/icons/fluo.gif")));
        FLUO_ICON.setDecorated(true);
        REFERENCE_ICON = new DecorableIcon(chartIcon);
        REFERENCE_ICON.setDecoration(
                new ImageIcon(QuickExafsViewer.class.getResource("/fr/soleil/fusion/quickexafs/icons/reference.gif")));
        REFERENCE_ICON.setDecorated(true);
    }

    // QUICK EXAFS PROPERTIES
    protected final Properties properties;

    // Components and model
    protected final QuickExafsModel model;
    protected FileView fileView;
    protected Chart chartViewer;
    protected JButton snapButton;
    protected JButton startButton;
    protected JButton clearSavedDatasButton;
    protected JButton updatePathButton;
    protected JLabel fileLabel;
    protected BasicFileTargetController fileController;
    protected AcquisitionManager acquisitionManager;
    protected final QuickExafsFileReader fileReader;
    protected final LogViewer logViewer;
    protected final LogAppender logAppender;
    protected JLabel displayTransmissionLabel;
    protected JCheckBox displayTransmissionCheckBox;
    protected JLabel displayFluoLabel;
    protected JCheckBox displayFluoCheckBox;
    protected JLabel displayReferenceLabel;
    protected JCheckBox displayReferenceCheckBox;
    protected JLabel displayFirstSpectrumLabel;
    protected JCheckBox displayFirstSpectrumCheckBox;
    protected JCheckBox displaySavedDatasCheckBox;
    protected JLabel delayLabel;
    protected JTextField delayTextField;
    protected JLabel directionLabel;
    protected JRadioButton forwardDirectionRadioButton;
    protected JRadioButton backwardDirectionRadioButton;
    protected ButtonGroup directionGroup;
    protected JLabel cidLabel;
    protected DynamicTextField cidTextField;
    protected JLabel thetaLabel;
    protected DynamicTextField thetaTextField;
    protected JLabel deltaTheta0Label;
    protected DynamicTextField deltaTheta0TextField;
    protected JLabel monoLabel;
    protected JTextField monoTextField;
    protected JLabel referenceValueLabel;
    protected JTextField referenceValueTextField;
    protected JLabel thresholdValueLabel;
    protected JTextField thresholdValueTextField;
    protected JLabel thresholdWidthLabel;
    protected JTextField thresholdWidthTextField;
    protected JLabel alternateAlgorithmLabel;
    protected JCheckBox alternateAlgorithmBox;
    protected JLabel deletePreviousCurvesLabel;
    protected JCheckBox deletePreviousCurvesBox;
    protected JLabel autoRefreshLabel;
    protected JCheckBox autoRefreshBox;
    protected JLabel edgeIndexLabel;
    protected JTextField edgeIndexTextField;

    // Threads
    protected SmartWorker loadAndDisplayDataWorker;
    protected final FileBufferingThread fileBufferingThread;

    // DataRecorder devices
    private String dataRecorder, scratchRecorder;
    private final UpdatePathBooleanTarget scratchModeTarget;

    // MISC
    protected int dataIndex;
    protected boolean started;
    protected final String applicationId;
    protected final Logger logger;
    protected boolean alternateAlgorithm;
    protected volatile boolean autoRefresh;
    protected boolean deletePreviousCurves;
    protected File lastIgnoredFile;
    protected long lastIgnoredFileDate, lastIgnoredFileCheck;
    protected final ITextTarget directoryAttributeTarget;
    protected final StringScalarBox stringScalarBox;
    protected final BooleanScalarBox booleanScalarBox;
    protected final Object directoryLock, connectionLock;
    protected final Timer directoryCheckTimer;
    protected final boolean debug;
    protected final SimpleDateFormat format;

    public QuickExafsViewer(final String applicationId, final Properties properties, final boolean alternateAlgorithm,
            final boolean autoRefresh, final boolean debug) throws NoSuchMethodException {
        super(new BorderLayout(5, 5));
        this.applicationId = applicationId == null ? Mediator.LOGGER_ACCESS : applicationId;
        logger = LoggerFactory.getLogger(this.applicationId);
        logViewer = new LogViewer(this.applicationId);
        logViewer.setBorder(BorderFactory.createTitledBorder("Logs"));
        logAppender = new LogAppender(logViewer, false);
        LogAppender.registerAppender(this.applicationId, logAppender);
        if (logger instanceof ch.qos.logback.classic.Logger) {
            final ch.qos.logback.classic.Logger loggerBack = (ch.qos.logback.classic.Logger) logger;
            if (debug) {
                loggerBack.setLevel(Level.ALL);
            }
        }
        this.properties = properties;
        model = new QuickExafsModel();
        model.addModelListener(this);
        this.alternateAlgorithm = alternateAlgorithm;
        this.autoRefresh = autoRefresh;
        this.deletePreviousCurves = autoRefresh;
        this.debug = debug;
        format = new SimpleDateFormat(IDateConstants.US_DATE_FORMAT);
        dataIndex = 0;
        started = false;
        resetLastIgnoredFile();
        fileReader = new QuickExafsFileReader(applicationId, properties, alternateAlgorithm, Double.NaN, Double.NaN);
        scratchModeTarget = new UpdatePathBooleanTarget();
        directoryAttributeTarget = new DirectoryUpdater();
        stringScalarBox = new StringScalarBox();
        booleanScalarBox = new BooleanScalarBox();
        directoryLock = new Object();
        connectionLock = new Object();

        loadAndDisplayDataWorker = null;
        fileBufferingThread = new FileBufferingThread();
        fileBufferingThread.start();

        initComponents();

        layoutComponents();
        initControllers();
        initConfigFromProperties();

        changeDirectory();

        updateComponentsForAlternateAlgorithm(false);
        updateComponentsForPreviousCurvesDeletion(false);
        updateComponentsForAutoRefresh(true);
        directoryCheckTimer = new Timer(getClass().getSimpleName() + CHECK_CURRENT_DIRECTORY_AND_CONTEXTUAL_DATA);
        directoryCheckTimer.schedule(new DirectoryCheckTask(), DIRECTORY_CHECK_PERIOD, DIRECTORY_CHECK_PERIOD);
    }

    protected void resetLastIgnoredFile() {
        lastIgnoredFile = null;
        lastIgnoredFileDate = 0;
        lastIgnoredFileCheck = 0;
    }

    protected void initConfigFromProperties() {
        String dataMergerDevice = properties.getProperty(DATA_MERGER_DEVICE);
        if (dataMergerDevice == null) {
            logger.error(NO_REGISTERED_DATA_MERGER_IN_PROPERTIES);
        }
        // CONTROLGUI-377, PROBLEM-2189: read DataRecorder and ScratchRecorder from properties file,
        // not from DataMerger properties
        String dataRecorderDevice = properties.getProperty(DATA_RECORDER_DEVICE);
        String scratchRecorderDevice = properties.getProperty(SCRATCH_RECORDER_DEVICE);
        this.dataRecorder = dataRecorderDevice;
        this.scratchRecorder = scratchRecorderDevice;
        String scatchAttribute = properties.getProperty(SCRATCH_MODE_ATTRIBUTE);
        TangoKey scratchModeKey = new TangoKey();
        TangoKeyTool.registerAttribute(scratchModeKey, dataMergerDevice, scatchAttribute);
        booleanScalarBox.connectWidget(scratchModeTarget, scratchModeKey);
    }

    protected String doubleToString(double val) {
        String str;
        double ref = Math.round(val);
        if (Math.abs(ref - val) < MathConst.PRECISION_LIMIT) {
            str = Long.toString((long) val);
        } else {
            str = Format.formatValue(val, "%f");
            int index = str.indexOf('.');
            if (index > -1 && str.charAt(str.length() - 1) == '0') {
                StringBuilder builder = new StringBuilder(str);
                // remove trailing 0
                while (builder.charAt(builder.length() - 1) == '0') {
                    builder.delete(builder.length() - 1, builder.length());
                }
                if (builder.length() == index + 1) {
                    builder.delete(builder.length() - 1, builder.length());
                }
                str = builder.toString();
            }
        }
        return str;
    }

    protected void initComponents() throws NoSuchMethodException {
        chartViewer = new Chart();
        chartViewer.setFreezePanelVisible(true);
        chartViewer.setManagementPanelVisible(false);
        chartViewer.setAutoHighlightOnLegend(!autoRefresh);
        fileLabel = new JLabel(NO_FILE_SELECTED, SwingConstants.CENTER);
        fileLabel.setFont(fileLabel.getFont().deriveFont(18f));
        fileLabel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

        startButton = new JButton(START, START_LOADING_ICON);
        startButton.addActionListener(this);
        startButton.setPreferredSize(new Dimension(40, 40));
        updatePathButton = new JButton(UPDATE_PATH, REFRESH_ICON);
        updatePathButton.addActionListener(this);
        updatePathButton.setPreferredSize(new Dimension(40, 40));

        snapButton = new JButton(SNAP, SNAPSHOT_ICON);
        snapButton.addActionListener(this);
        snapButton.setEnabled(false);
        snapButton.setPreferredSize(new Dimension(40, 40));
        clearSavedDatasButton = new JButton(CLEAR, CLEAR_LOG_ICON);
        clearSavedDatasButton.setPreferredSize(new Dimension(40, 40));
        displaySavedDatasCheckBox = new JCheckBox();
        displaySavedDatasCheckBox.setSelected(true);

        // INTER RETICULAR DISTANCE
        cidLabel = new JLabel(INTER_RETICULAR_DISTANCE_LABEL);
        cidLabel.setIcon(INTERRETICULAR_DISTANCE_ICON);
        cidTextField = new DynamicTextField(applicationId, model,
                model.getClass().getMethod(SET_INTER_RETICULAR_DISTANCE, new Class[] { Double.class }));
        cidTextField.setName(INTER_RETICULAR_DISTANCE_LABEL);
        cidTextField.setValue(model.getInterReticularDistance());

        // THETA
        thetaLabel = new JLabel(THETA_LABEL);
        thetaLabel.setIcon(THETA_ICON);
        thetaTextField = new DynamicTextField(applicationId, model,
                model.getClass().getMethod(SET_THETA, new Class[] { Double.class }));
        thetaTextField.setName(THETA_LABEL);
        thetaTextField.setValue(model.getTheta());

        // DELTA THETA0
        deltaTheta0Label = new JLabel(DELTA_THETA0_LABEL);
        deltaTheta0Label.setIcon(DELTA_THETA0_ICON);
        deltaTheta0TextField = new DynamicTextField(applicationId, model,
                model.getClass().getMethod("setDeltaTheta0", new Class[] { Double.class }));
        deltaTheta0TextField.setName(DELTA_THETA0_LABEL);
        deltaTheta0TextField.setValue(model.getDeltaTheta0());

        // DELAY
        delayLabel = new JLabel(DELAY);
        delayLabel.setIcon(DELAY_ICON);
        delayTextField = new DynamicTextField(applicationId, model,
                model.getClass().getMethod(SET_DELAY, new Class[] { Double.class }));
        delayTextField.setName(DELAY);
        delayTextField.setText(String.valueOf(model.getDelay()));

        displayTransmissionLabel = new JLabel("Show Transmission");
        displayTransmissionLabel.setIcon(TRANSMISSION_ICON);
        displayTransmissionCheckBox = new JCheckBox();
        displayTransmissionCheckBox.setSelected(true);
        displayTransmissionCheckBox.addActionListener(this);

        displayFluoLabel = new JLabel(SHOW_FLUORESCENCE);
        displayFluoLabel.setIcon(FLUO_ICON);
        displayFluoCheckBox = new JCheckBox();
        displayFluoCheckBox.addActionListener(this);

        displayReferenceLabel = new JLabel(SHOW_REFERENCE);
        displayReferenceLabel.setIcon(REFERENCE_ICON);
        displayReferenceCheckBox = new JCheckBox();
        displayReferenceCheckBox.addActionListener(this);

        displayFirstSpectrumLabel = new JLabel(SHOW_FIRST_SPECTRUM);
        displayFirstSpectrumLabel.setIcon(DISPLAY_FIRST_ICON);
        displayFirstSpectrumCheckBox = new JCheckBox();
        displayFirstSpectrumCheckBox.addActionListener(this);

        // Delete previous curves
        deletePreviousCurvesLabel = new JLabel(DELETE_PREVIOUS_CURVES, DELETE_PREVIOUS_CURVES_ICON, JLabel.LEFT);
        deletePreviousCurvesLabel.setToolTipText(DELETE_PREVIOUS_CURVES_TOOLTIP);
        deletePreviousCurvesBox = new JCheckBox();
        deletePreviousCurvesBox.setSelected(deletePreviousCurves);
        deletePreviousCurvesBox.addActionListener(this);

        // alternate algorithm
        alternateAlgorithmLabel = new JLabel(ALTERNATE_ALGORITHM, ALGORITHM_ICON, JLabel.LEFT);
        alternateAlgorithmBox = new JCheckBox();
        alternateAlgorithmBox.setSelected(alternateAlgorithm);
        alternateAlgorithmBox.addActionListener(this);
        referenceValueLabel = new JLabel(REFERENCE_VALUE);
        referenceValueLabel.setIcon(REFERENCE_VALUE_ICON);
        referenceValueTextField = new JTextField();
        final DocumentNumber referenceValueDocument = new DocumentNumber();
        referenceValueDocument.setAllowFloatValues(true);
        referenceValueDocument.setAllowScientificValues(false);
        referenceValueDocument.setAllowNegativeValues(false);
        referenceValueTextField.setDocument(referenceValueDocument);
        referenceValueTextField.setText(doubleToString(fileReader.getReferentValue()));
        referenceValueDocument.addDocumentListener(this);
        thresholdValueLabel = new JLabel(THRESHOLD_VALUE);
        thresholdValueLabel.setIcon(THRESHOLD_VALUE_ICON);
        thresholdValueTextField = new JTextField();
        final DocumentNumber thresholdValueDocument = new DocumentNumber();
        thresholdValueDocument.setAllowFloatValues(true);
        thresholdValueDocument.setAllowScientificValues(false);
        thresholdValueDocument.setAllowNegativeValues(false);
        thresholdValueTextField.setDocument(thresholdValueDocument);
        thresholdValueTextField.setText(doubleToString(fileReader.getThresholdValue()));
        thresholdValueDocument.addDocumentListener(this);
        thresholdValueTextField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                resetThresholdValue();
            }
        });
        thresholdWidthLabel = new JLabel(THRESHOLD_WIDTH);
        thresholdWidthLabel.setIcon(THRESHOLD_WIDTH_ICON);
        thresholdWidthTextField = new JTextField();
        final DocumentNumber thresholdWidthDocument = new DocumentNumber();
        thresholdWidthDocument.setAllowFloatValues(true);
        thresholdWidthDocument.setAllowScientificValues(false);
        thresholdWidthDocument.setAllowNegativeValues(false);
        thresholdWidthTextField.setDocument(thresholdWidthDocument);
        thresholdWidthTextField.setText(doubleToString(fileReader.getThresholdWidth()));
        thresholdWidthDocument.addDocumentListener(this);
        thresholdWidthTextField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                resetThresholdWidth();
            }
        });

        // auto refresh
        autoRefreshLabel = new JLabel(AUTO_REFRESH, AUTO_REFRESH_ICON, JLabel.LEFT);
        autoRefreshBox = new JCheckBox();
        autoRefreshBox.setSelected(autoRefresh);
        autoRefreshBox.addActionListener(this);
        edgeIndexLabel = new JLabel(EDGE_INDEX, EDGE_ICON, JLabel.LEFT);
        edgeIndexLabel.setToolTipText(EDGE_INDEX_TOOLTIP);
        edgeIndexTextField = new JTextField();
        final DocumentNumber edgeIndexDocument = new DocumentNumber();
        edgeIndexDocument.setAllowFloatValues(false);
        edgeIndexDocument.setAllowScientificValues(false);
        edgeIndexDocument.setAllowNegativeValues(false);
        edgeIndexTextField.setDocument(edgeIndexDocument);
        edgeIndexTextField.setText(Integer.toString(fileReader.getPreferedEdgeIndex() + 1));
        edgeIndexDocument.addDocumentListener(this);

        directionLabel = new JLabel(DIRECTION_FILTER);
        directionLabel.setIcon(DIRECTION_ICON);
        directionGroup = new ButtonGroup();
        forwardDirectionRadioButton = new JRadioButton(createSelectDirection(FORWARD, true));
        backwardDirectionRadioButton = new JRadioButton(createSelectDirection(BACKWARD, false));
        forwardDirectionRadioButton.setSelected(true);

        directionGroup.add(forwardDirectionRadioButton);
        directionGroup.add(backwardDirectionRadioButton);

        // MONO
        monoLabel = new JLabel(MONO);
        monoLabel.setIcon(MONO_ICON);
        monoTextField = new JTextField();
        monoTextField.setEditable(false);
    }

    protected AbstractAction createSelectDirection(final String title, final boolean forward) {
        final AbstractAction result = new AbstractAction(title) {
            private static final long serialVersionUID = -1227398085416287043L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                model.setForwardDirection(forward);
                fileReader.resetIndex();
            }
        };
        return result;
    }

    protected void layoutComponents() {
        final JSplitPane topBottomSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        final JSplitPane leftRightSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

        // PARAMETERS
        final JPanel parametersPanel = new JPanel(new GridBagLayout());
        int y = 0;
        y = layout2Components(parametersPanel, monoLabel, monoTextField, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, delayLabel, delayTextField, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, cidLabel, cidTextField, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, deltaTheta0Label, deltaTheta0TextField, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, thetaLabel, thetaTextField, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, deletePreviousCurvesLabel, deletePreviousCurvesBox, y,
                PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, autoRefreshLabel, autoRefreshBox, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, edgeIndexLabel, edgeIndexTextField, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, displayTransmissionLabel, displayTransmissionCheckBox, y,
                PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, displayFluoLabel, displayFluoCheckBox, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, displayReferenceLabel, displayReferenceCheckBox, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, displayFirstSpectrumLabel, displayFirstSpectrumCheckBox, y,
                PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, alternateAlgorithmLabel, alternateAlgorithmBox, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, thresholdValueLabel, thresholdValueTextField, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, thresholdWidthLabel, thresholdWidthTextField, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, referenceValueLabel, referenceValueTextField, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, directionLabel, forwardDirectionRadioButton, y, PARAMETERS_INSETS);
        y = layout2Components(parametersPanel, null, backwardDirectionRadioButton, y, PARAMETERS_INSETS_NO_GAP);

        // BUTTONS
        final JPanel buttonPanel = new JPanel(new GridBagLayout());
        final GridBagConstraints updatePathButtonConstraints = new GridBagConstraints();
        updatePathButtonConstraints.fill = GridBagConstraints.BOTH;
        updatePathButtonConstraints.gridx = 0;
        updatePathButtonConstraints.gridy = 0;
        updatePathButtonConstraints.weightx = 0.5;
        updatePathButtonConstraints.weighty = 1;
        updatePathButtonConstraints.insets = new Insets(4, 4, 4, 4);
        buttonPanel.add(updatePathButton, updatePathButtonConstraints);
        final GridBagConstraints startButtonConstraints = new GridBagConstraints();
        startButtonConstraints.fill = GridBagConstraints.BOTH;
        startButtonConstraints.gridx = 1;
        startButtonConstraints.gridy = 0;
        startButtonConstraints.weightx = 0.5;
        startButtonConstraints.weighty = 1;
        startButtonConstraints.insets = new Insets(4, 0, 4, 4);
        buttonPanel.add(startButton, startButtonConstraints);

        // FILE VIEW
        fileView = initFileView();
        fileView.setMinimumSize(new Dimension(0, 0));

        // WEST PANEL = FILE VIEW + PARAMETERS + BUTTONS
        final JPanel westPanel = new JPanel(new BorderLayout());
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitPane.setTopComponent(fileView);
        JScrollPane parametersPanelScrollPane = new JScrollPane(parametersPanel);
        parametersPanelScrollPane.setMinimumSize(new Dimension(0, 0));
        splitPane.setBottomComponent(parametersPanelScrollPane);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(0.5);
        splitPane.setResizeWeight(0.5);
        westPanel.add(splitPane, BorderLayout.CENTER);
        westPanel.add(buttonPanel, BorderLayout.SOUTH);
        westPanel.setMinimumSize(new Dimension(0, 0));

        // CENTER PANEL = CHART + FILE LABEL
        final JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
        centerPanel.add(fileLabel, BorderLayout.NORTH);
        centerPanel.add(chartViewer, BorderLayout.CENTER);
        centerPanel.setPreferredSize(new Dimension(900, 800));

        leftRightSplitPane.setLeftComponent(westPanel);
        leftRightSplitPane.setRightComponent(centerPanel);
        leftRightSplitPane.setDividerSize(8);
        leftRightSplitPane.setOneTouchExpandable(true);
        leftRightSplitPane.setPreferredSize(new Dimension(1000, 800));
        leftRightSplitPane.setDividerLocation(0.2);
        leftRightSplitPane.setResizeWeight(0.2);

        // LOG PANEL
        topBottomSplitPane.setTopComponent(leftRightSplitPane);
        topBottomSplitPane.setBottomComponent(logViewer);
        topBottomSplitPane.setDividerSize(8);
        topBottomSplitPane.setOneTouchExpandable(true);
        topBottomSplitPane.setDividerLocation(0.7);
        topBottomSplitPane.setResizeWeight(0.7);

        add(topBottomSplitPane);
    }

    protected int layout2Components(final JPanel panel, final JComponent title, final JComponent value, int y,
            final Insets insets) {
        if (title != null) {
            final GridBagConstraints titleConstraints = new GridBagConstraints();
            titleConstraints.fill = GridBagConstraints.BOTH;
            titleConstraints.gridx = 0;
            titleConstraints.gridy = y;
            titleConstraints.weightx = 0;
            titleConstraints.weighty = 0;
            if (insets != null) {
                titleConstraints.insets = insets;
            }
            panel.add(title, titleConstraints);
        }
        if (value != null) {
            final GridBagConstraints valueConstraints = new GridBagConstraints();
            valueConstraints.fill = GridBagConstraints.BOTH;
            valueConstraints.gridx = 1;
            valueConstraints.gridy = y;
            valueConstraints.weightx = 1;
            valueConstraints.weighty = 0;
            if (insets != null) {
                valueConstraints.insets = insets;
            }
            panel.add(value, valueConstraints);
        }
        return ++y;
    }

    protected void updateComponentsForAlternateAlgorithm(final boolean repaint) {
        alternateAlgorithm = alternateAlgorithmBox.isSelected();
        fileReader.setAlternateAlgorithm(alternateAlgorithm);
        referenceValueLabel.setVisible(alternateAlgorithm);
        referenceValueTextField.setVisible(alternateAlgorithm);
        thresholdValueLabel.setVisible(!alternateAlgorithm);
        thresholdValueTextField.setVisible(!alternateAlgorithm);
        thresholdWidthLabel.setVisible(!alternateAlgorithm);
        thresholdWidthTextField.setVisible(!alternateAlgorithm);
        if (repaint) {
            revalidate();
            repaint();
        }
    }

    protected void updateComponentsForPreviousCurvesDeletion(final boolean repaint) {
        deletePreviousCurves = deletePreviousCurvesBox.isSelected();
        chartViewer.setDataViewRemovingEnabled(!deletePreviousCurves);
        if (repaint) {
            revalidate();
            repaint();
        }
    }

    protected void updateComponentsForAutoRefresh(final boolean repaint) {
        reset(true);
        autoRefresh = autoRefreshBox.isSelected();
        updatePathButton.setVisible(autoRefresh);
        displayFirstSpectrumLabel.setVisible(autoRefresh);
        displayFirstSpectrumCheckBox.setVisible(autoRefresh);
        edgeIndexLabel.setVisible(!autoRefresh);
        edgeIndexTextField.setVisible(!autoRefresh);
        if (autoRefresh) {
            if (!attemptDirectoryTargetConnection(false)) {
                new Thread("Update path") {
                    @Override
                    public void run() {
                        updatePath();
                    };
                }.start();
            }
        } else {
            closeAutoConnection();
            startButton.setText("Show desired spectrum");
        }
        if (repaint) {
            revalidate();
            repaint();
        }
    }

    protected FileView initFileView() {
        final String directory = getCurrentDirectory();
        final FileView view = new FileView(applicationId, directory, FILE_EXTENSIONS);
        final Comparator<DefaultMutableTreeNode>[] filters = view.getSortFilters();
        for (final Comparator<DefaultMutableTreeNode> filter : filters) {
            if (filter instanceof NameFileNodeComparator && ((NameFileNodeComparator) filter).isAscending()) {
                view.setSelectedSortFilter(filter);
                break;
            }
        }
        final FileTreeModel model = (FileTreeModel) view.getFileTreeModel();
        model.addDirectoryListener(this);
        view.setFileBrowsingMode(FileBrowsingMode.MODE_TREE);
        final Dimension defaultSize = new Dimension(300, 200);
        view.setPreferredSize(defaultSize);
        view.getPathChanger().addActionListener(this);
        view.changePathTo(directory, debug);
        return view;
    }

    protected boolean isUsableTextValue(String value) {
        return (value != null) && (!StringScalarBox.DEFAULT_ERROR_STRING.equals(value));
    }

    protected String getDirectory(String recorderDevice, String directoryAttribute) {
        String directory = null;
        if (isUsableTextValue(recorderDevice) && isUsableTextValue(directoryAttribute)) {
            try {
                DeviceProxy drProxy = DeviceProxyFactory.get(recorderDevice);
                final DeviceAttribute targetDirAttr = drProxy.read_attribute(directoryAttribute);
                directory = targetDirAttr.extractString();
            } catch (final DevFailed e) {
                directory = null;
                DevFailedUtils.logDevFailed(e, logger);
            }
        }
        return directory;
    }

    protected String getRecorderDevice() {
        boolean scratch = scratchModeTarget.isSelected();
        String recorderDevice = scratch ? scratchRecorder : dataRecorder;
        // 2019-07-10 phone call (Confluence page 7050056): if ScratchRecorder is empty, read DataRecorderDeviceName
        if ((recorderDevice != null) && recorderDevice.trim().isEmpty()) {
            recorderDevice = dataRecorder;
        }
        return recorderDevice;
    }

    protected String getCurrentDirectory() {
        logger.trace("Recovering current directory...");
        String recorderDevice = getRecorderDevice();
        String directoryAttribute = properties.getProperty(TARGET_DIRECTORY_ATTRIBUTE);
        String directory = getDirectory(recorderDevice, directoryAttribute);
        if (directory == null) {
            if (!isUsableTextValue(recorderDevice)) {
                logger.trace("No daterecorder device");
            } else if (!isUsableTextValue(directoryAttribute)) {
                logger.trace("No directory attribute for device {}", recorderDevice);
            } else {
                logger.trace("Failed to read attribute " + recorderDevice + "/" + directoryAttribute);
            }
            directory = System.getProperty(USER_HOME);
        } else {
            try {
                if (!new File(directory).isDirectory()) {
                    logger.trace(directory + " is not identified as a valid directory");
                    directory = System.getProperty(USER_HOME);
                }
            } catch (final Exception e) {
                logger.trace("Failed to access " + directory);
                directory = System.getProperty(USER_HOME);
            }
        }
        logger.trace("Current directory is " + directory);
        return directory;
    }

    protected boolean attemptDirectoryTargetConnection(boolean trueIfAlreadyConnected) {
        boolean connected;
        if (autoRefresh && scratchModeTarget.isReady()) {
            String recorderDevice = getRecorderDevice();
            String directoryAttribute = properties.getProperty(TARGET_DIRECTORY_ATTRIBUTE);
            if (getDirectory(recorderDevice, directoryAttribute) == null) {
                connected = false;
            } else {
                synchronized (connectionLock) {
                    TangoKey key = new TangoKey();
                    TangoKeyTool.registerAttribute(key, recorderDevice, directoryAttribute);
                    if (stringScalarBox.isTargetConnected(directoryAttributeTarget, key)) {
                        connected = trueIfAlreadyConnected;
                    } else {
                        stringScalarBox.disconnectWidgetFromAll(directoryAttributeTarget);
                        connected = stringScalarBox.connectWidget(directoryAttributeTarget, key);
                    }
                }
            }
        } else {
            connected = false;
        }
        return connected;
    }

    protected void closeAutoConnection() {
        stringScalarBox.disconnectWidgetFromAll(directoryAttributeTarget);
    }

    protected boolean isContextualDataNeeded() {
        return (model.experimentChanged() || (model.getInterReticularDistance() == null)
                || (model.getDeltaTheta0() == null) || (model.getTheta() == null));
    }

    protected boolean isLoadableFile(File fileToLoad) {
        return ((fileToLoad != null) && fileToLoad.isFile());
    }

    protected File loadContextualAndFirstData(final String workingDir, final boolean showDialog) {
        File fileToLoad;
        if (isContextualDataNeeded()) {
            fileToLoad = fileReader.getFirstFileInDir(workingDir, fileView);
            if (isLoadableFile(fileToLoad)) {
                logger.trace(CONTEXTUAL_DATA_FOUND, fileToLoad.getName());
                loadContextualData(fileToLoad, showDialog);
                if (model.getCurrentFile() != null) {
                    fileReader.setFile(model.getCurrentFile());
                    fileToLoad = model.getCurrentFile();
                }
                final File file = fileToLoad;
                final Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        updateTreeSelection(file);
                    }
                };
                if (SwingUtilities.isEventDispatchThread()) {
                    runnable.run();
                } else {
                    SwingUtilities.invokeLater(runnable);
                }
            }
        } else {
            fileToLoad = null;
        }
        return fileToLoad;
    }

    protected boolean loadContextualData(final File contextFile, final boolean showDialog) {
        boolean treated = false;
        File fileToLoad = contextFile;
        if (isLoadableFile(fileToLoad)) {
            logger.trace(CONTEXTUAL_DATA_FOUND, fileToLoad.getName());
            fileReader.setFile(fileToLoad, false);

            boolean ok = loadSelectedMono(showDialog);
            ok = loadInterreticularDistance(showDialog) && ok;
            ok = loadTheta(showDialog) && ok;
            ok = loadDeltaTheta0(showDialog) && ok;
            if (ok) {
                fileReader.setFile(fileToLoad);
                final QuickExafsData firstData = readData(false, fileToLoad);
                if (firstData != null) {
                    firstData.computeData(model.getInterReticularDistance(), model.getDeltaTheta0(), model.getTheta());
                    model.setFirstData(firstData);
                    treated = true;
                }
            }
        }
        return treated;
    }

    protected boolean loadInterreticularDistance(final boolean showDialog) {
        boolean result = false;
        final Double distance = fileReader.loadInterreticularDistance();

        if (distance != null) {
            model.setInterReticularDistance(distance);

            cidTextField.setValue(model.getInterReticularDistance());
            logger.trace(INTER_RETICULAR_DISTANCE_FOUND, model.getInterReticularDistance());
            result = true;
        } else {
            if (showDialog) {
                JOptionPane.showMessageDialog(this, INTER_RETICULAR_DISTANCE_NOT_FOUND, CONTEXTUAL_DATA_MISSING,
                        JOptionPane.ERROR_MESSAGE);
            }
            cidTextField.setValue(null);
            cidTextField.setBackground(Color.RED);

        }
        return result;
    }

    protected boolean loadTheta(final boolean showDialog) {
        boolean result = false;
        final Double theta = fileReader.loadTheta();
        if (theta != null) {
            model.setTheta(theta);
            thetaTextField.setValue(model.getTheta());
            logger.trace(THETA_FOUND, model.getTheta());
            result = true;
        } else {
            if (showDialog) {
                JOptionPane.showMessageDialog(this, THETA_NOT_FOUND, CONTEXTUAL_DATA_MISSING,
                        JOptionPane.ERROR_MESSAGE);
            }
            thetaTextField.setValue(null);
            thetaTextField.setBackground(Color.RED);
        }
        return result;
    }

    protected boolean loadDeltaTheta0(final boolean showDialog) {
        boolean result = false;
        final Double deltaTheta0 = fileReader.loadDeltaTheta0();

        if (deltaTheta0 != null) {
            model.setDeltaTheta0(deltaTheta0);

            deltaTheta0TextField.setValue(model.getDeltaTheta0());
            logger.trace(DELTA_THETA0_FOUND, model.getDeltaTheta0());
            result = true;
        } else {

            if (showDialog) {
                JOptionPane.showMessageDialog(this, DELTA_THETA0_NOT_FOUND, CONTEXTUAL_DATA_MISSING,
                        JOptionPane.ERROR_MESSAGE);
            }
            deltaTheta0TextField.setValue(null);
            deltaTheta0TextField.setBackground(Color.RED);
        }
        return result;
    }

    protected boolean loadSelectedMono(final boolean showDialog) {
        boolean result = false;
        final String mono = fileReader.loadMono();
        if (mono != null) {
            monoTextField.setText(mono.toString());
            model.setMono(mono);
            logger.trace(MONO_FOUND, mono);
            result = true;
            monoTextField.setBackground(Color.WHITE);
        } else {
            if (showDialog) {
                JOptionPane.showMessageDialog(this, MONO_NOT_FOUND, CONTEXTUAL_DATA_MISSING, JOptionPane.ERROR_MESSAGE);
            }
            monoTextField.setText(ObjectUtils.EMPTY_STRING);
            monoTextField.setBackground(Color.RED);
        }
        return result;
    }

    protected double[][] getTransmissionData(final QuickExafsData data) {
        final double[][] arrayData = new double[2][];
        if (data != null) {
            arrayData[IChartViewer.Y_INDEX] = data.getMux();
            arrayData[IChartViewer.X_INDEX] = data.getEnergy();
        }
        return arrayData;
    }

    protected double[][] getFluoData(final QuickExafsData data) {
        final double[][] arrayData = new double[2][];
        if (data != null) {
            arrayData[IChartViewer.Y_INDEX] = data.getFluoMux();
            arrayData[IChartViewer.X_INDEX] = data.getEnergy();
        }
        return arrayData;
    }

    protected double[][] getReferenceData(final QuickExafsData data) {
        final double[][] arrayData = new double[2][];
        if (data != null) {
            arrayData[IChartViewer.Y_INDEX] = data.getRefMux();
            arrayData[IChartViewer.X_INDEX] = data.getEnergy();
        }
        return arrayData;
    }

    protected void loadCurrentDataInChart() {
        final TreeMap<String, Object> chartData = new TreeMap<>();
        final QuickExafsData quickExafsData = model.getCurrentData();

        int dataIndexForColor;
        if (autoRefresh) {
            dataIndexForColor = ++dataIndex;
        } else {
            dataIndexForColor = fileReader.getPreferedEdgeIndex() + 1;
        }

        final boolean transmission = displayTransmissionCheckBox.isSelected();
        final boolean fluo = displayFluoCheckBox.isSelected();
        final boolean reference = displayReferenceCheckBox.isSelected();
        final Map<String, Integer> axisMap = new HashMap<>();
        if (transmission) {
            final double[][] data = getTransmissionData(quickExafsData);
            chartData.put(buildDataViewNameOrId(false, dataIndexForColor, null), data);
        }
        if (fluo) {
            final double[][] data = getFluoData(quickExafsData);
            final String dataViewId = buildDataViewNameOrId(false, dataIndexForColor, FLUO_SUFFIX);
            chartData.put(dataViewId, data);
            if (transmission) {
                axisMap.put(dataViewId, IChartViewer.Y2);
            } else {
                axisMap.put(dataViewId, IChartViewer.Y1);
            }
        }
        if (reference) {
            final double[][] data = getReferenceData(quickExafsData);
            final String dataViewId = buildDataViewNameOrId(false, dataIndexForColor, REFERENCE_SUFFIX);
            chartData.put(dataViewId, data);
            axisMap.put(dataViewId, IChartViewer.Y1);
        }
        if (displayFirstSpectrumCheckBox.isSelected()) {
            final QuickExafsData firstData = model.getFirstData();
            double[][] arrayData;
            if (transmission) {
                arrayData = getTransmissionData(firstData);
                final String firstSpectrumId = buildFirstSpectrumId(false);
                chartData.put(firstSpectrumId, arrayData);
                chartViewer.setDataViewDisplayName(firstSpectrumId, buildFirstSpectrumName(false));
            }
            if (fluo) {
                arrayData = getFluoData(firstData);
                final String firstSpectrumId = buildFirstSpectrumId(true);
                chartData.put(firstSpectrumId, arrayData);
                chartViewer.setDataViewDisplayName(firstSpectrumId, buildFirstSpectrumName(true));
                if (transmission) {
                    axisMap.put(firstSpectrumId, IChartViewer.Y2);
                } else {
                    axisMap.put(firstSpectrumId, IChartViewer.Y1);
                }
            }
            if (reference) {
                arrayData = getReferenceData(firstData);
                final String firstSpectrumId = buildFirstSpectrumId(true);
                chartData.put(firstSpectrumId, arrayData);
                chartViewer.setDataViewDisplayName(firstSpectrumId, buildFirstSpectrumName(true));
                axisMap.put(firstSpectrumId, IChartViewer.Y1);
            }

            final List<QuickExafsData> savedDatas = model.getSavedDatas();
            for (final QuickExafsData quickExafsSavedData : savedDatas) {
                if (transmission) {
                    arrayData = getTransmissionData(quickExafsSavedData);
                    chartData.put(buildSavedSpectrumId(quickExafsSavedData.getIndex(), false), arrayData);
                }
                if (fluo) {
                    final String id = buildSavedSpectrumId(quickExafsSavedData.getIndex(), true);
                    arrayData = getFluoData(quickExafsSavedData);
                    chartData.put(id, arrayData);
                    if (transmission) {
                        axisMap.put(id, IChartViewer.Y2);
                    } else {
                        axisMap.put(id, IChartViewer.Y1);
                    }
                }
                if (reference) {
                    final String id = buildSavedSpectrumId(quickExafsSavedData.getIndex(), true);
                    arrayData = getReferenceData(quickExafsSavedData);
                    chartData.put(id, arrayData);
                    axisMap.put(id, IChartViewer.Y1);
                }
            }
        }

        chartViewer.setAxisName(ENERGY, IChartViewer.X);
        chartViewer.setAxisName(MUX, IChartViewer.Y1);

        if (deletePreviousCurves) {
            if (transmission) {
                final String dataViewId = buildDataViewNameOrId(false, dataIndexForColor, null);
                chartViewer.setDataViewCometeColor(dataViewId, TRANSMISSION_COLOR);
                chartViewer.setDataViewDisplayName(dataViewId, buildDataViewNameOrId(true, dataIndexForColor, null));
            }
            if (fluo) {
                final String dataViewId = buildDataViewNameOrId(false, dataIndexForColor, FLUO_SUFFIX);
                chartViewer.setDataViewCometeColor(dataViewId, FLUO_COLOR);
                chartViewer.setDataViewDisplayName(dataViewId,
                        buildDataViewNameOrId(true, dataIndexForColor, FLUO_SUFFIX));
            }
            if (reference) {
                final String dataViewId = buildDataViewNameOrId(false, dataIndexForColor, REFERENCE_SUFFIX);
                chartViewer.setDataViewCometeColor(dataViewId, REFERENCE_COLOR);
                chartViewer.setDataViewDisplayName(dataViewId,
                        buildDataViewNameOrId(true, dataIndexForColor, REFERENCE_SUFFIX));
            }
            chartViewer.setData(chartData);
        } else {
            if (transmission) {
                final String dataViewId = buildDataViewNameOrId(false, dataIndexForColor, null);
                if (TRANSMISSION_COLOR.equals(chartViewer.getDataViewCometeColor(dataViewId))) {
                    chartViewer.setDataViewPlotProperties(dataViewId, null);
                }
                chartViewer.setDataViewDisplayName(dataViewId, buildDataViewNameOrId(true, dataIndexForColor, null));
            }
            if (fluo) {
                final String dataViewId = buildDataViewNameOrId(false, dataIndexForColor, FLUO_SUFFIX);
                if (FLUO_COLOR.equals(chartViewer.getDataViewCometeColor(dataViewId))) {
                    chartViewer.setDataViewPlotProperties(dataViewId, null);
                }
                chartViewer.setDataViewDisplayName(dataViewId,
                        buildDataViewNameOrId(true, dataIndexForColor, FLUO_SUFFIX));
            }
            if (reference) {
                final String dataViewId = buildDataViewNameOrId(false, dataIndexForColor, REFERENCE_SUFFIX);
                if (REFERENCE_COLOR.equals(chartViewer.getDataViewCometeColor(dataViewId))) {
                    chartViewer.setDataViewPlotProperties(dataViewId, null);
                }
                chartViewer.setDataViewDisplayName(dataViewId,
                        buildDataViewNameOrId(true, dataIndexForColor, REFERENCE_SUFFIX));
            }
            chartViewer.addData(chartData);
        }
        for (final Entry<String, Integer> entry : axisMap.entrySet()) {
            chartViewer.setDataViewAxis(entry.getKey(), entry.getValue());
        }
        axisMap.clear();
    }

    protected String buildDataViewNameOrId(final boolean name, final int dataIndexForColor, final String suffix) {
        final StringBuilder builder = new StringBuilder();
        if (name) {
            builder.append(model.getCurrentFile().getName());
        } else {
            builder.append(model.getCurrentFile().getAbsolutePath());
        }
        builder.append(SEPARATOR).append(dataIndexForColor);
        if (suffix != null) {
            builder.append(suffix);
        }
        return builder.toString();
    }

    protected String buildSavedSpectrumId(final int index, final boolean fluo) {
        final StringBuilder builder = new StringBuilder(SPECTRUM_PREFIX);
        builder.append(index);
        if (fluo) {
            builder.append(FLUO_SUFFIX);
        }
        return builder.toString();
    }

    protected String buildFirstSpectrumId(final boolean fluo) {
        final StringBuilder nameBuilder = new StringBuilder(FIRST_SPECTRUM);
        if (fluo) {
            nameBuilder.append(FLUO_SUFFIX);
        }
        final File f = model.getCurrentFile();
        if (f != null) {
            nameBuilder.append(OPEN_PARENTHESIS).append(f.getAbsolutePath()).append(')');
        }
        return nameBuilder.toString();
    }

    protected String buildFirstSpectrumName(final boolean fluo) {
        final StringBuilder nameBuilder = new StringBuilder(FIRST_SPECTRUM);
        if (fluo) {
            nameBuilder.append(FLUO_SUFFIX);
        }
        final File f = model.getCurrentFile();
        if (f != null) {
            nameBuilder.append(OPEN_PARENTHESIS).append(f.getName()).append(')');
        }
        return nameBuilder.toString();
    }

    protected void updatePath(String path) {
        fileView.getFileTree().setEnabled(false);
        fileView.changePathTo(path == null ? getCurrentDirectory() : path, debug);
        changeDirectory();
    }

    protected void updatePath() {
        updatePath(null);
    }

    protected QuickExafsData readData(final boolean fromPreferedEdgeIndex, File file) {
        QuickExafsData result = null;
        final double energyConversionFactor = Double.parseDouble(properties.getProperty(ENERGY_CONVERSION_FACTOR));

        final double[][] datas = fileReader.loadQuickExafsData(model.isForwardDirection(), fromPreferedEdgeIndex);
        if (datas != null) {
            result = new QuickExafsData(applicationId, datas, energyConversionFactor,
                    QuickExafsModel.getExperiment(file));
        }
        return result;
    }

    protected void initControllers() {
        // link file controller with file view
        if (acquisitionManager == null) {
            acquisitionManager = new AcquisitionManager(fileReader);
        }
        if (fileController == null) {
            fileController = new BasicFileTargetController();
        }
        if (acquisitionManager.getCdmaReader() instanceof IFileTarget) {
            fileController.addLink(fileView.getFileSource(), (IFileManager) acquisitionManager.getCdmaReader());
        }
        fileController.addLink(fileView.getFileSource(), this);
    }

    protected void initWorker() {
        loadAndDisplayDataWorker = new SmartWorker(autoRefresh);
    }

    protected void reset(final boolean updateStartButton) {
        logger.trace(RESET_ALL);
        // Change directory -> Reset everything

        model.reset();

        // Reset View
        started = false;

        cancelLoading(updateStartButton);
        fileReader.setMonoKey(null);

        if (deletePreviousCurves) {
            final Collection<String> ids = new ArrayList<>();
            ids.addAll(chartViewer.getData().keySet());
            // reset without loosing freezes
            chartViewer.clearExpressions();
            chartViewer.setData(null);
            for (final String id : ids) {
                chartViewer.setDataViewPlotProperties(id, null);
            }
            ids.clear();
        }
        dataIndex = 0;
        fileView.getFileTree().setEnabled(true);
    }

    protected void changeDirectory() {
        File toSelect = null;
        reset(autoRefresh);
        final File workingDirFile = fileView.getPathChanger().getSelectedFile();
        if (workingDirFile != null) {
            final String workingDir = workingDirFile.getAbsolutePath();
            if (workingDir != null) {
                logger.trace(NEW_WORKING_DIRECTORY_IS + workingDir);
                toSelect = loadContextualAndFirstData(workingDir, false);
            }
        }
        selectNode(toSelect);
    }

    protected void selectNode(File file) {
        if (isValidFile(file)) {
            FileTreeModel model = (FileTreeModel) fileView.getFileTreeModel();
            TreePath path = fileView.getFileTree().getSelectionPath();
            DefaultMutableTreeNode node = (path == null ? null : (DefaultMutableTreeNode) path.getLastPathComponent());
            DefaultMutableTreeNode toSelect = model.getNodeForFile(file);
            if (!ObjectUtils.sameObject(node, toSelect)) {
                if (toSelect == null) {
                    fileView.getFileTree().clearSelection();
                } else {
                    fileView.getFileTree().setSelectionPath(new TreePath(toSelect.getPath()));
                }
            }
        }
    }

    protected void runWorker() {
        if (started) {
            final File file = model.getCurrentFile();
            initWorker();
            logger.trace(EXEC);
            if (autoRefresh) {
                // Modify Load Button
                startButton.setText(STOP);
                startButton.setIcon(STOP_LOADING_ICON);
            }
            loadAndDisplayDataWorker.execute();
            if (autoRefresh) {
                fileBufferingThread.removeFiles(file);
            }
            if (ObjectUtils.sameObject(file, lastIgnoredFile)) {
                resetLastIgnoredFile();
            }
        }
    }

    protected void cancelLoading(final boolean updateStartButton) {
        logger.trace(CANCEL);
        final SmartWorker loadAndDisplayDataWorker = this.loadAndDisplayDataWorker;
        if (loadAndDisplayDataWorker != null) {
            loadAndDisplayDataWorker.cancel();
        }
        this.loadAndDisplayDataWorker = null;
        if (updateStartButton) {
            startButton.setText(START);
            startButton.setIcon(START_LOADING_ICON);
        }
        snapButton.setEnabled(false);
        resetLastIgnoredFile();
        fileBufferingThread.clearBuffer();
        fileReader.close();
    }

    protected void showFirstSpectrum() {
        final TreeMap<String, Object> chartData = new TreeMap<>();
        final QuickExafsData quickExafsData = model.getFirstData();
        if (quickExafsData != null) {
            if (displayTransmissionCheckBox.isSelected()) {
                final double[][] data = getTransmissionData(quickExafsData);
                final String firstSpectrumId = buildFirstSpectrumId(false);
                chartData.put(firstSpectrumId, data);
                chartViewer.setDataViewDisplayName(firstSpectrumId, buildFirstSpectrumName(false));
            }
            if (displayFluoCheckBox.isSelected()) {
                final double[][] data = getFluoData(quickExafsData);
                final String firstSpectrumId = buildFirstSpectrumId(true);
                chartData.put(firstSpectrumId, data);
                chartViewer.setDataViewDisplayName(firstSpectrumId, buildFirstSpectrumName(true));
                if (displayTransmissionCheckBox.isSelected()) {
                    chartViewer.setDataViewAxis(firstSpectrumId, IChartViewer.Y2);
                } else {
                    chartViewer.setDataViewAxis(firstSpectrumId, IChartViewer.Y1);
                }
            }
            if (displayReferenceCheckBox.isSelected()) {
                final double[][] data = getReferenceData(quickExafsData);
                final String firstSpectrumId = buildFirstSpectrumId(true);
                chartData.put(firstSpectrumId, data);
                chartViewer.setDataViewDisplayName(firstSpectrumId, buildFirstSpectrumName(true));
                chartViewer.setDataViewAxis(firstSpectrumId, IChartViewer.Y1);
            }
            chartViewer.addData(chartData);
        }
    }

    protected void hideFirstSpectrum() {
        final List<String> idsToRemove = new ArrayList<>();
        if (displayFluoCheckBox.isSelected()) {
            idsToRemove.add(buildFirstSpectrumId(true));
        }
        if (displayReferenceCheckBox.isSelected()) {
            idsToRemove.add(buildFirstSpectrumId(true));
        }
        if (displayTransmissionCheckBox.isSelected()) {
            idsToRemove.add(buildFirstSpectrumId(false));
        }
        chartViewer.removeData(idsToRemove);
    }

    protected void updateFirstSpectrumVisibility() {
        if (displayFirstSpectrumCheckBox.isSelected()) {
            showFirstSpectrum();
        } else {
            hideFirstSpectrum();
        }
    }

    protected boolean isValidFile(final File file) {
        return (file != null) && file.exists() && (!file.isDirectory())
                && FileTreeRenderer.NEXUS_EXTENSION.equals(FileUtils.getExtension(file)) && file.length() > 0;
    }

    protected boolean isWorkerReady() {
        return loadAndDisplayDataWorker == null || loadAndDisplayDataWorker.hasFinished();

    }

    // ////////////// //
    // IModelListener //
    // ////////////// //

    @Override
    public void fileChanged() {
        final File file = model.getCurrentFile();
        fileLabel.setText(file == null ? NO_FILE_SELECTED : file.getName());
        if (isValidFile(file)) {
            fileReader.setFile(file);
        }
    }

    // //////////////// //
    // DocumentListener //
    // //////////////// //

    @Override
    public void removeUpdate(final DocumentEvent e) {
        treatUpdate(e);
    }

    @Override
    public void insertUpdate(final DocumentEvent e) {
        treatUpdate(e);
    }

    @Override
    public void changedUpdate(final DocumentEvent e) {
        treatUpdate(e);
    }

    protected boolean shoudlResetField(JTextField field, Supplier<Double> method, double value) {
        double reference = method.get();
        return value != reference && Math.abs(value - reference) > MathConst.PRECISION_LIMIT && (!field.hasFocus());
    }

    protected void resetValue(JTextField field, Supplier<Double> method) {
        final int position = field.getCaretPosition();
        field.getDocument().removeDocumentListener(this);
        field.setText(doubleToString(method.get()));
        try {
            field.setCaretPosition(position);
        } catch (final Exception e) {
            // Ignore this exception
        }
        field.getDocument().addDocumentListener(this);
    }

    protected void resetReferentValue() {
        resetValue(referenceValueTextField, fileReader::getReferentValue);
    }

    protected void resetThresholdValue() {
        resetValue(thresholdValueTextField, fileReader::getThresholdValue);
    }

    protected void resetThresholdWidth() {
        resetValue(thresholdWidthTextField, fileReader::getThresholdWidth);
    }

    protected void treatUpdate(final DocumentEvent evt) {
        try {
            if (evt.getDocument() == referenceValueTextField.getDocument()) {
                final double value = Double.parseDouble(referenceValueTextField.getText().trim());
                fileReader.setReferentValue(Math.min(value, Short.MAX_VALUE - Short.MIN_VALUE));
                if (value != fileReader.getReferentValue()) {
                    new Thread(RESET_REFERENT_VALUE) {
                        @Override
                        public void run() {
                            resetReferentValue();
                        }
                    }.start();
                }
            } else if (evt.getDocument() == thresholdValueTextField.getDocument()) {
                final double value = Double.parseDouble(thresholdValueTextField.getText().trim());
                fileReader.setThresholdValue(value);
                if (shoudlResetField(thresholdValueTextField, fileReader::getThresholdValue, value)) {
                    new Thread(RESET_THRESHOLD_VALUE) {
                        @Override
                        public void run() {
                            resetThresholdValue();
                        }
                    }.start();
                }
            } else if (evt.getDocument() == thresholdWidthTextField.getDocument()) {
                final double value = Double.parseDouble(thresholdWidthTextField.getText().trim());
                fileReader.setThresholdWidth(value);
                if (shoudlResetField(thresholdWidthTextField, fileReader::getThresholdWidth, value)) {
                    new Thread(RESET_THRESHOLD_WIDTH) {
                        @Override
                        public void run() {
                            resetThresholdWidth();
                        }
                    }.start();
                }
            } else if (evt.getDocument() == edgeIndexTextField.getDocument()) {
                final int index = (int) Math.min(Integer.MAX_VALUE,
                        Long.parseLong(edgeIndexTextField.getText().trim()));
                fileReader.setPreferedEdgeIndex(index - 1);
                if (index != fileReader.getPreferedEdgeIndex() + 1) {
                    new Thread(RESET_EDGE_INDEX) {

                        @Override
                        public void run() {
                            final int position = edgeIndexTextField.getCaretPosition();
                            edgeIndexTextField.getDocument().removeDocumentListener(QuickExafsViewer.this);
                            edgeIndexTextField.setText(Integer.toString(fileReader.getPreferedEdgeIndex() + 1));
                            try {
                                edgeIndexTextField.setCaretPosition(position);
                            } catch (final Exception e) {
                                // Ignore this exception
                            }
                            edgeIndexTextField.getDocument().addDocumentListener(QuickExafsViewer.this);
                        }
                    }.start();
                }
            }
        } catch (final Exception e) {
            // nothing to do
        }
    }

    // ////////////// //
    // ActionListener //
    // ////////////// //

    @Override
    public void actionPerformed(final ActionEvent e) {
        if (e != null) {
            if (e.getSource() == alternateAlgorithmBox) {
                updateComponentsForAlternateAlgorithm(true);
            } else if (e.getSource() == deletePreviousCurvesBox) {
                updateComponentsForPreviousCurvesDeletion(true);
            } else if (e.getSource() == autoRefreshBox) {
                updateComponentsForAutoRefresh(true);
            } else if (e.getSource() == fileView.getPathChanger()
                    && e.getActionCommand() == JFileChooser.APPROVE_SELECTION) {
                // Do this in a separate Thread in order not to lock FileView
                new Thread(getClass().getSimpleName() + " change directory") {
                    @Override
                    public void run() {
                        changeDirectory();
                    }
                }.start();
            } else if (e.getSource() == displayFirstSpectrumCheckBox) {
                updateFirstSpectrumVisibility();
            } else if (e.getSource() == updatePathButton) {
                updatePath();
            } else if (e.getSource() == snapButton) {
                model.saveData(dataIndex);
            } else if (e.getSource() == clearSavedDatasButton) {
                model.clearSavedDatas();
            } else if (e.getSource() == startButton) {
                if (started) {
                    cancelLoading(autoRefresh);
                    started = false;
                    fileView.getFileTree().setEnabled(true);
                } else {
                    started = true;
                    fileView.getFileTree().setEnabled(false);
                    File file = model.getCurrentFile();
                    if (autoRefresh && ((file == null) || (!file.exists()))) {
                        Object root = fileView.getFileTreeModel().getRoot();
                        if (root instanceof DefaultMutableTreeNode) {
                            DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) root;
                            Object userObject = rootNode.getUserObject();
                            if (userObject instanceof String) {
                                file = fileReader.getFirstFileInDir((String) userObject, fileView);
                            }
                        }
                    }
                    model.reset();
                    model.setCurrentFile(file);
                    if (isValidFile(file)) {
                        fileReader.setFile(file);
                    }
                    dataIndex = 0;
                    if (autoRefresh) {
                        if (isValidFile(file)) {
                            File[] tmp = file.getParentFile().listFiles();
                            fileReader.sortFiles(tmp);
                            int index = -1;
                            boolean found = false;
                            for (File toCheck : tmp) {
                                index++;
                                if (file.equals(toCheck)) {
                                    found = true;
                                    break;
                                }
                            }
                            if (found) {
                                tmp = Arrays.copyOfRange(tmp, index, tmp.length);
                            }
                            filesAdded(true, tmp);
                            started = true;
                        } else {
                            started = false;
                            fileView.getFileTree().setEnabled(true);
                        }
                    } else {
                        runWorker();
                        started = false;
                    }
                }
            }
        }
    }

    // ///////////////// //
    // DirectoryListener //
    // ///////////////// //

    @Override
    public void filesAdded(final File... addedFiles) {
        filesAdded(false, addedFiles);
    }

    protected void filesAdded(final boolean firstFileNow, final File... addedFiles) {
        File[] toCheck = addedFiles;
        if (started && autoRefresh && (toCheck != null) && (toCheck.length > 0)) {
            final Collection<File> effectiveFiles = new ArrayList<>(toCheck.length);
            for (final File file : toCheck) {
                if (isValidFile(file)) {
                    effectiveFiles.add(file);
                }
            }
            toCheck = effectiveFiles.toArray(new File[effectiveFiles.size()]);
            if (toCheck.length > 0) {
                fileReader.sortFiles(toCheck);
                final File file = toCheck[0];
                final File[] files = fileReader.sortFiles(file.getParent());
                if ((files != null) && (files.length > 0)) {
                    // Never treat last file immediately, as it might still being modified by acquisition process.
                    int index = -1;
                    for (int i = files.length - 1; i >= 0; i--) {
                        if (file.equals(files[i])) {
                            index = i;
                            break;
                        }
                    }
                    final int excludedMax = files.length - 1;
                    if (firstFileNow && (index != excludedMax)) {
                        index++;
                        if (ObjectUtils.sameObject(file, model.getCurrentFile())) {
                            runWorker();
                        } else {
                            setFile(file);
                        }
                    }
                    if (index > -1) {
                        File lastIgnoredFile = this.lastIgnoredFile;
                        if (index < excludedMax) {
                            File[] toBuffer;
                            if (lastIgnoredFile == null) {
                                toBuffer = new File[excludedMax - index];
                                System.arraycopy(files, index, toBuffer, 0, excludedMax - index);
                            } else {
                                toBuffer = new File[excludedMax + 1 - index];
                                toBuffer[0] = lastIgnoredFile;
                                System.arraycopy(files, index, toBuffer, 1, excludedMax - index);
                            }
                            fileBufferingThread.addFiles(toBuffer);
                        }
                        lastIgnoredFile = files[excludedMax];
                        if (!ObjectUtils.sameObject(lastIgnoredFile, this.lastIgnoredFile)) {
                            if (lastIgnoredFile != null) {
                                lastIgnoredFileCheck = System.currentTimeMillis();
                                lastIgnoredFileDate = lastIgnoredFile.lastModified();
                            }
                            this.lastIgnoredFile = lastIgnoredFile;
                            if (lastIgnoredFile == null) {
                                lastIgnoredFileCheck = 0;
                                lastIgnoredFileDate = 0;
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void filesRemoved(final File... removedFiles) {
        // What should I do ?
    }

    // /////////// //
    // IFileTarget //
    // /////////// //

    @Override
    public void addMediator(final Mediator<?> mediator) {
    }

    @Override
    public void removeMediator(final Mediator<?> mediator) {
    }

    @Override
    public void setFile(final File file) {
        if (isValidFile(file)) {
            // The file seems to be a valid nexus file
            if ((model.getCurrentFile() == null) || (file.isFile()) && (!file.equals(model.getCurrentFile()))) {
                if (!started || isWorkerReady()) {
                    fileLabel.setText(file.getName());
                    model.setCurrentFile(file);
                    if (!updateTreeSelection(file)) {
                        // for some reason, the file selection did not work
                        fileView.selectFile(file);
                    }
                    dataIndex = 0;
                    if (started) {
                        runWorker();
                    }
                } else if (autoRefresh) {
                    fileBufferingThread.addFiles(file);
                }
            } else {
                // file is already loaded
                fileBufferingThread.removeFiles(file);
            }
        }
    }

    /**
     * Tries to select a {@link File} in {@link FileView}'s tree.
     *
     * @param file The {@link File} to select
     * @return Whether the {@link File} was successfully selected.
     */
    protected boolean updateTreeSelection(final File file) {
        boolean selectionDone = false;
        // Update Tree Selection if needed
        final TreePath tp = fileView.getFileTree().getSelectionPath();
        final DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) fileView.getFileTree().getModel().getRoot();
        FileNode nodeToSelect = null;
        if (rootNode != null) {
            boolean updateSelection = true;
            if (tp != null) {
                final DefaultMutableTreeNode node = (DefaultMutableTreeNode) tp.getLastPathComponent();
                if (node instanceof FileNode && ObjectUtils.sameObject(file, ((FileNode) node).getData())) {
                    updateSelection = false;
                    selectionDone = true;
                }
            }
            if (updateSelection) {
                final Enumeration<?> nodeEnum = rootNode.children();
                while (nodeEnum.hasMoreElements()) {
                    final FileNode tmp = (FileNode) nodeEnum.nextElement();
                    if (ObjectUtils.sameObject(tmp.getData(), file)) {
                        nodeToSelect = tmp;
                        break;
                    }
                }
            }
        }
        if (nodeToSelect != null) {
            fileView.getFileTree().setSelectionPath(new TreePath(nodeToSelect.getPath()));
            Container parent = fileView.getFileTree().getParent();
            if (parent instanceof JViewport) {
                parent = ((JViewport) parent).getParent();
                if (parent instanceof JScrollPane) {
                    try {
                        Rectangle bounds = fileView.getFileTree()
                                .getPathBounds(fileView.getFileTree().getSelectionPath());
                        fileView.getFileTree().scrollRectToVisible(bounds);
                        fileView.repaint();
                    } catch (Exception e) {
                        // nothing to do: we don't care if we couldn't scroll
                    }
                }
            }
            selectionDone = true;
        }
        return selectionDone;
    }

    // //////////// //
    // Main program //
    // //////////// //

    public static void main(final String[] args) throws IOException, NoSuchMethodException {
        Locale.setDefault(Locale.US);
        final Splash splash = new Splash(GUIUtilities.DEFAULT_UTILITIES.getIcon("fr.soleil.cdma.box.Splash"),
                Color.BLACK);
        splash.pack();
        splash.setLocationRelativeTo(null);
        splash.setAlwaysOnTop(true);
        splash.setTitle(QuickExafsViewer.class.getSimpleName());
        splash.setCopyright("\u00a9 SOLEIL Synchrotron");
        splash.progress(0);
        splash.setMessage("Analyzing application arguments...");
        boolean alternateAlgorithm = false, autorefresh = true, debug = false;
        String alternateStr = "alternate", norefreshStr = "norefresh", debugStr = "debug";
        for (final String arg : args) {
            if (alternateStr.equalsIgnoreCase(arg.trim())) {
                alternateAlgorithm = true;
            } else if (norefreshStr.equalsIgnoreCase(arg.trim())) {
                autorefresh = false;
            } else if (debugStr.equalsIgnoreCase(arg.trim())) {
                debug = true;
            }
        }
        splash.progress(25);
        splash.setMessage("Loading properties...");
        final Properties properties = new Properties();
        properties.load(ClassLoader.getSystemResourceAsStream("quickExafs.properties"));

        splash.progress(50);
        splash.setMessage("Creating UI...");
        Toolkit.getDefaultToolkit().setDynamicLayout(true);
        Rectangle maxBounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        final QuickExafsViewer viewer = new QuickExafsViewer(CometeUtils.generateIdForClass(QuickExafsViewer.class),
                properties, alternateAlgorithm, autorefresh, debug);

        splash.progress(75);
        splash.setMessage("Displaying Window...");
        // LET'S BUILD A COOL FRAME
        final JFrame f = new JFrame(QuickExafsViewer.TITLE);
        f.setSize(new Dimension(Math.min(1280, maxBounds.width), Math.min(1024, maxBounds.height)));
        f.setVisible(true);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        // PUT THE VIEWER IN THAT FRAME
        f.setContentPane(viewer);
        splash.setMessage("done");
        splash.progress(100);
        splash.setVisible(false);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * {@link SwingWorker} for data loading
     * 
     * @author GIRARDOT
     */
    protected class SmartWorker extends SwingWorker<QuickExafsData, Void> {

        protected volatile boolean cancelled;
        protected volatile boolean finished;
        protected final boolean autoRefresh;
        protected SmartWorker worker;

        public SmartWorker(final boolean autoRefresh) {
            super();
            this.autoRefresh = autoRefresh;
            this.cancelled = false;
            this.finished = false;
            this.worker = null;
        }

        public void cancel() {
            cancelled = true;
            if (worker != null) {
                worker.cancel();
            }
        }

        public boolean hasFinished() {
            return finished || (worker != null) && worker.hasFinished();
        }

        @Override
        protected QuickExafsData doInBackground() throws Exception {
            if (model.getCurrentFile() == null) {
                model.setCurrentFile(fileReader.getFirstFileInDir(fileView.getCurrentPath(), fileView));
            }
            File file = model.getCurrentFile();
            QuickExafsData data = null;
            if (file != null) {
                // If needed
                loadContextualAndFirstData(file.isDirectory() ? file.getAbsolutePath() : file.getParent(), false);
                if (isContextualDataNeeded()) {
                    loadContextualData(file, false);
                }
                Thread.sleep(Math.round(model.getDelay() * 1000));
                if (!cancelled) {
                    data = readData(!autoRefresh, file);
                    if (data != null) {
                        data.computeData(model.getInterReticularDistance(), model.getDeltaTheta0(), model.getTheta());
                    }
                    if (!cancelled) {
                        model.setData(data);
                    }
                }
            }
            return data;
        }

        @Override
        protected void done() {
            if (!cancelled) {
                final NotifyAndIncrementWorker notifyAndIncrementWorker = new NotifyAndIncrementWorker();
                notifyAndIncrementWorker.execute();
            }
        }

        protected class NotifyAndIncrementWorker extends SwingWorker<Void, Void> {
            @Override
            protected Void doInBackground() throws Exception {
                if (!cancelled) {
                    loadCurrentDataInChart();
                }
                return null;
            }

            @Override
            protected void done() {
                if (autoRefresh && fileReader.hasNextValues(model.isForwardDirection(), !autoRefresh) && started
                        && !cancelled) {
                    worker = new SmartWorker(autoRefresh);
                    if (!cancelled) {
                        worker.execute();
                    }
                } else {
                    finished = true;
                    if (!autoRefresh) {
                        fileView.getFileTree().setEnabled(true);
                    }
                    fileReader.close();
                }
            }
        }
    }

    /**
     * {@link Thread} that manages the files to load later
     * 
     * @author GIRARDOT
     */
    protected class FileBufferingThread extends Thread {

        protected static final int SLEEPING_PERIOD = 2000;

        protected final Collection<File> fileBuffer;

        public FileBufferingThread() {
            super(DateUtil.elapsedTimeToStringBuilder(new StringBuilder(QuickExafsViewer.class.getSimpleName())
                    .append(" file buffering Thread (files checked every "), SLEEPING_PERIOD).toString());
            fileBuffer = new LinkedHashSet<>();
        }

        public void addFiles(final File... files) {
            if (files != null && files.length > 0) {
                synchronized (fileBuffer) {
                    for (final File file : files) {
                        if (isValidFile(file)) {
                            fileBuffer.add(file);
                        }
                    }
                }
            }
        }

        public void removeFiles(final File... files) {
            if (files != null && files.length > 0) {
                synchronized (fileBuffer) {
                    for (final File file : files) {
                        if (isValidFile(file)) {
                            fileBuffer.remove(file);
                        }
                    }
                }
            }
        }

        public void clearBuffer() {
            synchronized (fileBuffer) {
                fileBuffer.clear();
            }
        }

        @Override
        public void run() {
            while (true) {
                File file = null;
                synchronized (fileBuffer) {
                    if (!fileBuffer.isEmpty()) {
                        // get first file in buffer
                        for (final File tmp : fileBuffer) {
                            file = tmp;
                            break;
                        }
                    }
                }
                if (started && (file != null)) {
                    setFile(file);
                }
                // in auto refresh mode, add lastIgnoredFile if it is stable since more than 10s
                // and there is no more file to treat.
                synchronized (fileBuffer) {
                    if (autoRefresh && fileBuffer.isEmpty() && (lastIgnoredFile != null)
                            && (System.currentTimeMillis() - lastIgnoredFileCheck > 10000)) {
                        if (lastIgnoredFile.lastModified() == lastIgnoredFileDate) {
                            addFiles(lastIgnoredFile);
                            resetLastIgnoredFile();
                        } else {
                            lastIgnoredFileDate = lastIgnoredFile.lastModified();
                            lastIgnoredFileCheck = System.currentTimeMillis();
                        }
                    }
                }
                try {
                    sleep(SLEEPING_PERIOD);
                } catch (final InterruptedException e) {
                    logger.trace("sleep interrupted");
                }
            }
        }
    }

    /**
     * An {@link ITextTarget} that is expected to be connected to datarecorder's directory attribute, in order to update
     * FileView's directory.
     * 
     * @author GIRARDOT
     */
    protected class DirectoryUpdater implements ITextTarget {

        private volatile String path;

        public DirectoryUpdater() {
            path = null;
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public String getText() {
            return path;
        }

        @Override
        public void setText(String text) {
            if (debug) {
                logger.debug(new StringBuilder("+ ").append(format.format(new Date())).append(" ")
                        .append(getClass().getSimpleName()).append(" received path: '").append(text).append("' from ")
                        .append(getRecorderDevice()).append('/')
                        .append(properties.getProperty(TARGET_DIRECTORY_ATTRIBUTE)).toString());
            }
            if (autoRefresh && isUsableTextValue(text)) {
                if (debug) {
                    logger.debug(new StringBuilder("+ ").append(format.format(new Date())).append(" ")
                            .append(getClass().getSimpleName()).append(" -> waiting for directory lock").toString());
                }
                synchronized (directoryLock) {
                    if (debug) {
                        logger.debug(new StringBuilder("+ ").append(format.format(new Date())).append(" ")
                                .append(getClass().getSimpleName()).append(" directory lock acquired").toString());
                    }
                    if (autoRefresh) {
                        path = text;
                        final File workingDirFile = fileView.getPathChanger().getSelectedFile();
                        if (debug) {
                            logger.debug(new StringBuilder("+ ").append(format.format(new Date())).append(" ")
                                    .append(getClass().getSimpleName()).append(" working directory to test: '")
                                    .append(workingDirFile == null ? null : workingDirFile.getAbsolutePath())
                                    .append("'").toString());
                        }
                        if (workingDirFile != null) {
                            if (!workingDirFile.getAbsolutePath().equals(text)) {
                                if (debug) {
                                    logger.debug(new StringBuilder("+ ").append(format.format(new Date())).append(" ")
                                            .append(getClass().getSimpleName()).append(" apply path: '").append(text)
                                            .append("'").toString());
                                }
                                updatePath(text);
                            }
                        }
                    }
                }
            } else {
                if (debug) {
                    logger.debug(new StringBuilder("+ ").append(format.format(new Date())).append(" ")
                            .append(getClass().getSimpleName()).append(" no path to apply -> forget it'").toString());
                }
                path = null;
            }
        }
    }

    /**
     * A {@link TimerTask} that will check whether datarecorder's declared directory matches current one, and whether
     * contextual data are up to date.
     * 
     * @author GIRARDOT
     */
    protected class DirectoryCheckTask extends TimerTask {

        @Override
        public void run() {
            try {
                if (debug) {
                    logger.debug(new StringBuilder("\u00a9 ").append(format.format(new Date())).append(" ")
                            .append(getClass().getSimpleName()).append(" woke up").toString());
                }
                if (autoRefresh) {
                    if (debug) {
                        logger.debug(new StringBuilder("\u00a9 ").append(format.format(new Date())).append(" ")
                                .append(getClass().getSimpleName()).append(" waiting for directory lock").toString());
                    }
                    synchronized (directoryLock) {
                        if (debug) {
                            logger.debug(new StringBuilder("\u00a9 ").append(format.format(new Date())).append(" ")
                                    .append(getClass().getSimpleName()).append(" directory lock acquired").toString());
                        }
                        if (autoRefresh) {
                            // workingDirFile represents file view directory
                            final File workingDirFile = fileView.getPathChanger().getSelectedFile();
                            if (debug) {
                                logger.debug(new StringBuilder("\u00a9 ").append(format.format(new Date())).append(" ")
                                        .append(getClass().getSimpleName()).append(" working directory to test: '")
                                        .append(workingDirFile == null ? null : workingDirFile.getAbsolutePath())
                                        .append("'").toString());
                            }
                            if (workingDirFile != null) {
                                // dir represents datarecorder's declared directory
                                String dir = directoryAttributeTarget.getText();
                                if (debug) {
                                    logger.debug(new StringBuilder("\u00a9 ").append(format.format(new Date()))
                                            .append(" ").append(getClass().getSimpleName())
                                            .append(" should be compared with: '").append(dir).append("'").toString());
                                }
                                if (dir != null) {
                                    if (workingDirFile.getAbsolutePath().equals(dir)) {
                                        // Here, the FileView is on the right directory.
                                        // So, let's check contextual data
                                        String path = fileReader.getWorkingPath();
                                        if (debug) {
                                            logger.debug(new StringBuilder("\u00a9 ").append(format.format(new Date()))
                                                    .append(" ").append(getClass().getSimpleName())
                                                    .append(" fileReader working path: '").append(path).append("'")
                                                    .toString());
                                        }
                                        if (path != null) {
                                            File toCheck = new File(path).getParentFile();
                                            if (!ObjectUtils.sameObject(workingDirFile, toCheck)) {
                                                // contextual data is not up to date with current directory
                                                if (debug) {
                                                    logger.debug(new StringBuilder("\u00a9 ")
                                                            .append(format.format(new Date())).append(" ")
                                                            .append(getClass().getSimpleName())
                                                            .append(" Contextual data is not up to date with current directory! -> apply path: '")
                                                            .append(dir).append("'").toString());
                                                }
                                                updatePath(dir);
                                            }
                                        }
                                    } else {
                                        // FileView is not on the right directory
                                        if (debug) {
                                            logger.debug(new StringBuilder("\u00a9 ").append(format.format(new Date()))
                                                    .append(" ").append(getClass().getSimpleName())
                                                    .append(" FileView is not on the right directory! -> apply path: '")
                                                    .append(dir).append("'").toString());
                                        }
                                        updatePath(dir);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Encountered an error while checking current directory", e);
            }
        }

    }

    /**
     * An {@link IBooleanTarget} used to receive whether to use scratch mode.
     * 
     * @author GIRARDOT
     */
    protected class UpdatePathBooleanTarget implements IBooleanTarget {
        protected volatile Boolean selected;

        public UpdatePathBooleanTarget() {
            selected = null;
        }

        public boolean isReady() {
            return selected != null;
        }

        @Override
        public boolean isSelected() {
            return selected == null ? false : selected.booleanValue();
        }

        @Override
        public void setSelected(boolean selected) {
            this.selected = Boolean.valueOf(selected);
            attemptDirectoryTargetConnection(true);
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

    }
}
