package fr.soleil.fusion.quickexafs.model;

public interface IModelListener {

	public void fileChanged();
}
