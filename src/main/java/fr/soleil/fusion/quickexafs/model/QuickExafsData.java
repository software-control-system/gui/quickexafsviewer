package fr.soleil.fusion.quickexafs.model;

import org.slf4j.LoggerFactory;

import fr.soleil.data.mediator.Mediator;

public class QuickExafsData {

    private int index;
    private volatile boolean cancel;
    private final double energyConversionFactor;

    // INPUT PARAMETERS
    private final double[] deltaTheta;
    private final double[] intensity0;
    private final double[] intensity1;
    private final double[] intensity2;
    private final double[] fluo;

    // OUTPUT PARAMETERS
    private double[] energy;
    private double[] mux;
    private double[] fluoMux;
    private double[] refMux;
    // private double[] muxs;

    protected final String applicationId;
    private String experiment;

    public QuickExafsData(final String applicationId, final double[][] arrays, final double energyConversionFactor,
            final String experiment) {
        this.applicationId = applicationId == null ? Mediator.LOGGER_ACCESS : applicationId;
        if (arrays != null) {
            deltaTheta = arrays[0];
            intensity0 = arrays[1];
            intensity1 = arrays[2];
            intensity2 = arrays[3];
            fluo = arrays[4];
        } else {
            deltaTheta = null;
            intensity0 = null;
            intensity1 = null;
            intensity2 = null;
            fluo = null;
        }
        this.energyConversionFactor = energyConversionFactor;
        this.index = 0;
        this.experiment = experiment;
    }

    public String getExperiment() {
        return experiment;
    }

    public double[] getDeltaTheta() {
        return deltaTheta;
    }

    public double[] getIntensity0() {
        return intensity0;
    }

    public double[] getIntensity1() {
        return intensity1;
    }

    public double[] getIntensity2() {
        return intensity2;
    }

    public double[] getFluo() {
        return fluo;
    }

    public void computeData(final double interreticularDistance, final double deltaTheta0, final double theta) {
        // local copy to avoid potential thread concurrency problems
        final double[] deltaTheta = this.deltaTheta;
        final double[] intensity0 = this.intensity0;
        final double[] intensity1 = this.intensity1;
        final double[] intensity2 = this.intensity2;
        final double[] fluo = this.fluo;
        final double energyConversionFactor = this.energyConversionFactor;
        // local initialization to avoid potential thread concurrency problems
        double[] tmpEnergy;
        double[] tmpMux;
        double[] tmpFluoMux;
        double[] tmpRefMux;
        if ((deltaTheta == null) || (intensity0 == null)) {
            tmpEnergy = null;
            tmpMux = null;
            tmpFluoMux = null;
            tmpRefMux = null;
        } else {
            tmpEnergy = new double[deltaTheta.length];
            if (intensity1 == null) {
                tmpMux = null;
                tmpRefMux = null;
                if (fluo == null) {
                    tmpFluoMux = null;
                    for (int i = 0; i < deltaTheta.length && !cancel; i++) {
                        tmpEnergy[i] = computeEnergy(deltaTheta, energyConversionFactor, interreticularDistance,
                                deltaTheta0, i);
                    }
                } else {
                    tmpFluoMux = new double[deltaTheta.length];
                    for (int i = 0; i < deltaTheta.length && !cancel; i++) {
                        tmpEnergy[i] = computeEnergy(deltaTheta, energyConversionFactor, interreticularDistance,
                                deltaTheta0, i);
                        tmpFluoMux[i] = computeFluoMux(intensity0, fluo, i);
                    }
                }
            } else if (fluo == null) {
                tmpFluoMux = null;
                tmpMux = new double[deltaTheta.length];
                if (intensity2 == null) {
                    tmpRefMux = null;
                    for (int i = 0; i < deltaTheta.length && !cancel; i++) {
                        tmpEnergy[i] = computeEnergy(deltaTheta, energyConversionFactor, interreticularDistance,
                                deltaTheta0, i);
                        tmpMux[i] = computeMux(intensity0, intensity1, i);
                    }
                } else {
                    tmpRefMux = new double[deltaTheta.length];
                    for (int i = 0; i < deltaTheta.length && !cancel; i++) {
                        tmpEnergy[i] = computeEnergy(deltaTheta, energyConversionFactor, interreticularDistance,
                                deltaTheta0, i);
                        tmpMux[i] = computeMux(intensity0, intensity1, i);
                        tmpRefMux[i] = computeReferenceMux(intensity1, intensity2, i);
                    }
                }
            } else if (intensity2 == null) {
                tmpRefMux = null;
                tmpFluoMux = new double[deltaTheta.length];
                tmpMux = new double[deltaTheta.length];
                for (int i = 0; i < deltaTheta.length && !cancel; i++) {
                    tmpEnergy[i] = computeEnergy(deltaTheta, energyConversionFactor, interreticularDistance,
                            deltaTheta0, i);
                    tmpMux[i] = computeMux(intensity0, intensity1, i);
                    tmpFluoMux[i] = computeFluoMux(intensity0, fluo, i);
                }
            } else {
                tmpFluoMux = new double[deltaTheta.length];
                tmpMux = new double[deltaTheta.length];
                tmpRefMux = new double[deltaTheta.length];
                for (int i = 0; i < deltaTheta.length && !cancel; i++) {
                    tmpEnergy[i] = computeEnergy(deltaTheta, energyConversionFactor, interreticularDistance,
                            deltaTheta0, i);
                    tmpMux[i] = computeMux(intensity0, intensity1, i);
                    tmpFluoMux[i] = computeFluoMux(intensity0, fluo, i);
                    tmpRefMux[i] = computeReferenceMux(intensity1, intensity2, i);
                }
            }
        }
        energy = tmpEnergy;
        mux = tmpMux;
        fluoMux = tmpFluoMux;
        refMux = tmpRefMux;
    }

    protected double computeEnergy(final double[] deltaTheta, final double energyConversionFactor,
            final double interreticularDistance, final double deltaTheta0, final int index) {
        return energyConversionFactor
                / (2 * interreticularDistance * Math.sin(Math.toRadians(deltaTheta[index] + deltaTheta0)));
    }

    protected double computeMux(final double[] intensity0, final double[] intensity1, final int index) {
        double result;
        if ((index < intensity1.length) && (index < intensity0.length)) {
            try {
                result = Math.log(intensity0[index] / intensity1[index]);
            } catch (final ArithmeticException ae) {
                result = Double.NaN;
                LoggerFactory.getLogger(applicationId).error("Error in Log(Intensity0/Intensity1)", ae);
            }
        } else {
            result = Double.NaN;
        }
        return result;
    }

    protected double computeFluoMux(final double[] intensity0, final double[] fluo, final int index) {
        double result;
        if ((index < fluo.length) && (index < intensity0.length)) {
            try {
                result = fluo[index] / intensity0[index];
            } catch (final ArithmeticException ae) {
                result = Double.NaN;
                LoggerFactory.getLogger(applicationId).error("Error in fluo / intensity0", ae);
            }
        } else {
            result = Double.NaN;
        }
        return result;
    }

    protected double computeReferenceMux(final double[] intensity1, final double[] intensity2, final int index) {
        double result;
        if ((index < intensity1.length) && (index < intensity2.length)) {
            try {
                result = Math.log(intensity1[index] / intensity2[index]);
            } catch (final ArithmeticException ae) {
                result = Double.NaN;
                LoggerFactory.getLogger(applicationId).error("Error in Log(Intensity1/Intensity2)", ae);
            }
        } else {
            result = Double.NaN;
        }
        return result;
    }

    public double[] getEnergy() {
        return energy;
    }

    public void setEnergy(final double[] energy) {
        this.energy = energy;
    }

    public double[] getMux() {
        return mux;
    }

    public double[] getFluoMux() {
        return fluoMux;
    }

    public double[] getRefMux() {
        return refMux;
    }

    public void cancel() {
        this.cancel = true;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(final int index) {
        this.index = index;
    }
}
