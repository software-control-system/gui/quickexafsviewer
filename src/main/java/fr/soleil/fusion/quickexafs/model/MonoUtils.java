package fr.soleil.fusion.quickexafs.model;

import fr.soleil.lib.project.ObjectUtils;

public final class MonoUtils {

    private static final String SPACE = " ";
    private static final String TAB = "\t";

    private static final String MONO1 = "mono1";
    private static final String MONO2 = "mono2";
    private static final String MONO3 = "mono3";

    private static final String MONO1_DIRECTION_KEY = "mono1_direction";
    private static final String MONO2_DIRECTION_KEY = "mono2_direction";
    private static final String MONO3_DIRECTION_KEY = "mono3_direction";

    public static String findMonoDirectionKey(String mono) {
        String key;
        if (mono == null) {
            key = null;
        } else {
            String monoExpr = mono.replace(SPACE, ObjectUtils.EMPTY_STRING).replace(TAB, ObjectUtils.EMPTY_STRING)
                    .trim().toLowerCase();
            if (monoExpr.isEmpty()) {
                key = null;
            } else if (monoExpr.startsWith(MONO1)) {
                key = MONO1_DIRECTION_KEY;
            } else if (monoExpr.startsWith(MONO2)) {
                key = MONO2_DIRECTION_KEY;
            } else if (monoExpr.startsWith(MONO3)) {
                key = MONO3_DIRECTION_KEY;
            } else {
                key = null;
            }
        }
        return key;
    }
}
