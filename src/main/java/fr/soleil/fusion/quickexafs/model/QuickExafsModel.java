package fr.soleil.fusion.quickexafs.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.soleil.lib.project.ObjectUtils;

public class QuickExafsModel {

    private QuickExafsData currentData = null;
    private QuickExafsData firstData;
    private String firstDataExperiment = ObjectUtils.EMPTY_STRING;
    private final List<QuickExafsData> savedDatas = new ArrayList<>();
    private File currentFile;
    private String currentExperiment = ObjectUtils.EMPTY_STRING;
    private final List<IModelListener> listeners = new ArrayList<>();
    private Double delay = 1.0;
    private boolean forwardDirection = true;
    private Double interReticularDistance;
    private Double deltaTheta0;
    private Double theta;
    private String mono;

    public static String getExperiment(File file) {
        String experiment;
        if (file == null) {
            experiment = ObjectUtils.EMPTY_STRING;
        } else {
            experiment = file.getName();
            int separator = experiment.lastIndexOf('_');
            if (separator < 0) {
                separator = experiment.lastIndexOf('.');
            }
            if (separator > -1) {
                experiment = experiment.substring(0, separator);
            }
        }
        return experiment;
    }

    public String getFirstDataExperiment() {
        return firstDataExperiment;
    }

    public void setData(QuickExafsData data) {
        currentData = data;
    }

    public QuickExafsData getFirstData() {
        return firstData;
    }

    public void saveData(int index) {
        currentData.setIndex(index);
        savedDatas.add(currentData);
    }

    public void clearSavedDatas() {
        savedDatas.clear();
    }

    public List<QuickExafsData> getSavedDatas() {
        return savedDatas;
    }

    public void setFirstData(QuickExafsData firstData) {
        String firstDataExperiment;
        if (firstData == null) {
            firstDataExperiment = ObjectUtils.EMPTY_STRING;
        } else {
            firstDataExperiment = firstData.getExperiment();
        }
        if (firstDataExperiment == null) {
            firstDataExperiment = ObjectUtils.EMPTY_STRING;
        }
        this.firstData = firstData;
        this.firstDataExperiment = firstDataExperiment;
    }

    public QuickExafsData getCurrentData() {
        return currentData;
    }

    public void setInterReticularDistance(Double interReticularDistance) {
        this.interReticularDistance = interReticularDistance;
    }

    public Double getInterReticularDistance() {
        return this.interReticularDistance;
    }

    public File getCurrentFile() {
        return currentFile;
    }

    public void setCurrentFile(File currentFile) {
        String currentExperiment = getExperiment(currentFile);
        this.currentFile = currentFile;
        this.currentExperiment = currentExperiment;
        fireFileChanged();
    }

    public boolean experimentChanged() {
        String firstDataExp = getFirstDataExperiment();
        return firstDataExp.isEmpty() || (!ObjectUtils.sameObject(firstDataExp, currentExperiment));
    }

    public void addModelListener(IModelListener listener) {
        listeners.add(listener);
    }

    private void fireFileChanged() {
        for (IModelListener listener : listeners) {
            listener.fileChanged();
        }

    }

    public Double getDelay() {
        return delay;
    }

    public void setDelay(Double delay) {
        this.delay = delay;
    }

    public boolean isForwardDirection() {
        return forwardDirection;
    }

    public void setForwardDirection(boolean forwardDirection) {
        this.forwardDirection = forwardDirection;
    }

    public Double getDeltaTheta0() {
        return deltaTheta0;
    }

    public void setDeltaTheta0(Double deltaTheta0) {
        this.deltaTheta0 = deltaTheta0;
    }

    public Double getTheta() {
        return theta;
    }

    public void setTheta(Double theta) {
        this.theta = theta;
    }

    public void setMono(String mono) {
        this.mono = mono;
    }

    public String getMono() {
        return this.mono;
    }

    public void reset() {
        currentData = null;
        firstData = null;
        deltaTheta0 = null;
        theta = null;
        interReticularDistance = null;
        currentFile = null;
        mono = null;
    }

}
